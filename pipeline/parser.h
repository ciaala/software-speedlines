#define BOOST_SPIRIT_DEBUG
#ifndef FF_PARSER_H
#define FF_PARSER_H




#include "configbuilder.h"
#include <string>
#include <list>
#include <map>
#include <iostream>


#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/support_istream_iterator.hpp>
#include <boost/spirit/include/qi_parse.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/spirit/include/phoenix_bind.hpp>
#include <boost/spirit/include/support_unused.hpp>

#include <boost/variant/recursive_variant.hpp>

namespace spirit = boost::spirit;
namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;
namespace phoenix = boost::phoenix;

using ascii::char_;
using qi::lit;
using qi::_1;
using qi::double_;

using qi::on_error;
using qi::accept;
using phoenix::val;
using phoenix::construct;
using phoenix::bind;
using qi::lexeme;
using qi::eol;
using ascii::blank;
using ascii::space;


typedef std::list<std::string>::iterator s_list_it;

BOOST_FUSION_ADAPT_STRUCT(
	ff::pstage,
	(std::string, type)
	(std::string, name)
	(map_v_type, paramv)
)

namespace ff {
	



class
ConfigParser : public qi::grammar<spirit::istream_iterator >
{
private:
	qi::rule<spirit::istream_iterator, map_v_type()> paramv;
	qi::rule<spirit::istream_iterator, pair_v_type()> pairv;
	qi::rule<spirit::istream_iterator, value_type()> value;
	
	qi::rule<spirit::istream_iterator, std::string()> qstring;
	qi::rule<spirit::istream_iterator, std::string()> sstring;
	qi::rule<spirit::istream_iterator> skip;
	qi::rule<spirit::istream_iterator> start;
	qi::rule<spirit::istream_iterator, std::list<std::string>() > connect;
	qi::rule<spirit::istream_iterator, pstage()> stage;
	qi::rule<spirit::istream_iterator> comment;

private:
	ConfigBuilder * builder;
	const std::string *current_key;
public:
	ConfigParser(ConfigBuilder *builder);
	bool parseStream(spirit::istream_iterator &begin, spirit::istream_iterator &end);
};

};
#endif