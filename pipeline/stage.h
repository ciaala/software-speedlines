/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FF_STAGE_H
#define FF_STAGE_H


#include <set>
#include "algorithm.h"
#include "queueobject.h"
#include "queue.h"
#include "thread.h"

namespace ff {
template<class ObjectType>
class Stage : public Runnable
{
	typedef Stage<ObjectType> TStage;
private:

	boost::mutex _mutex;
   // std::list<Thread*> runners;
    Algorithm<ObjectType> *_algorithm;
	std::set<TStage*> nexts;

    Queue<ObjectType> queue;

	void propagate(ObjectType object, QueueObject<ObjectType>* oldQueueObject);
	
public:
    Stage(Algorithm<ObjectType> * algorithm);
    void shutdown();
	
    bool appendStage(TStage *next);

    void appendObject(QueueObject<ObjectType> *object);

	
	void setup(Thread* runner);
    void run(Thread* runner);
	Queue<ObjectType> *getQueue();
	Algorithm<ObjectType> *algorithm();
	Parameters *parameters();
};

}
#include "stage.cpp"
#endif // FF_STAGE_H
