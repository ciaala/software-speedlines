/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "queue.h"


namespace ff {
template<class ObjectType>
Queue<ObjectType>::Queue()
{
	
}

template<class ObjectType>
void Queue<ObjectType>::push_back(QueueObject<ObjectType> *object)
{
	{
		boost::lock_guard<boost::mutex> lock(mut);
		object->enqueTime();
		objects.push_back(object);
	}
	cond.notify_all();
}

template<class ObjectType>
QueueObject<ObjectType>* Queue<ObjectType>::pop_front()
{
	
	QueueObject<ObjectType> *q_o;
	{
		boost::unique_lock<boost::mutex> lock(mut);
		while ( objects.size() == 0 ) {
			cond.wait(lock);
		}
		q_o = objects.front();
		objects.pop_front();

	}
	q_o->startTime();
	return q_o;
	
}

template<class ObjectType>
Delaying& Queue<ObjectType>::delaying()
{
	return _delaying;
}

};