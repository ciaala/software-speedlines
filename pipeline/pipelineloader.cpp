/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "pipelineloader.h"
#include <parser.h>
#include <fstream>
#include <iostream>
#include <string>
#include <utility>
/*#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/qi_parse.hpp>
#include <boost/spirit/include/qi_string.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_bind.hpp>*/
#include "parser.h"
#include "parameters.h"

namespace ff {

	




// template <typename Iterator>
// struct pipeline_parser : qi::grammar<Iterator,unsigned()> {
// 	qi::rule<Iterator, std::string(), ascii::space_type> mystring;
// 	mystring %= +(char_ - ';' - ',');
// 	
// 	pipeline_parser()  : {
// 	}
// }

// namespace qi = boost::spirit::qi;
// namespace ascii = boost::spirit::ascii;
// namespace phoenix = boost::phoenix;
// using qi::lit;
// using ascii::char_;
// using ascii::space;
// using qi::phrase_parse;
// using qi::lexeme;
// using phoenix::ref;
// using boost::spirit::_val;
// using boost::spirit::_1;
// using qi::double_;

// template <typename Iterator>
// bool new_configure( Iterator first, Iterator last,
// 					std::string& s1, std::string& s2,
// 					std::map<std::string,std::string> &strings,
// 					std::map<std::string,double> &doubles) {
// 	
// 	//using qi::no_skip;
// 	typedef std::pair<std::string, std::string> spair_type;
// 	typedef std::pair<std::string, double> dpair_type;
// 	
// 	qi::rule<Iterator, std::string(), ascii::space_type> mystring, qstring;
// 	
// 	qi::rule<Iterator, spair_type(), ascii::space_type> string_pair;
// 	qi::rule<Iterator, dpair_type(), ascii::space_type> double_pair;
// 	
// 	mystring %= +(char_ - ',' - ';');
// 	qstring %= lexeme['"' >> +(char_ - '"') >> '"'];
// 	
// 	string_pair %= (qstring >> '=' >> qstring);
// 	double_pair %= (qstring >> '=' >> double_);
// 
// 	//list_pair %= *( string_pair[bind(std::map::insert, _val ] || double_pair );
// 	bool r = phrase_parse( first, last,
// 						   (
// 								lit("new")
// 								>> mystring[ref(s1) = _1]
// 								>> lit(",")
// 								>> mystring[ref(s2) = _1]
// 								>> lit(":") >> lit("{")
// 								>> *( string_pair[bind(std::map<std::string,std::string>::insert, ref(strings), _val) ] || double_pair[bind(std::map<std::string,double>::insert, ref(doubles), _val) ] )
// 								>> lit("}")
// 						   ),
// 						space );
// 	//&Parameters::setString, ref(p),
// 	if (first != last )
// 		return false;
// 	return r;
// }
// 
// 



// template <typename Iterator>
// bool connect_parser( Iterator first, Iterator last, std::string& s1, std::string& s2) {
// 
// 
// 
//     qi::rule<Iterator, std::string(), ascii::space_type> mystring;
//     mystring %= +(char_ - ';' - ',' - ' ');
// 	
//     bool r = phrase_parse( first, last,
//                            (
//                                lit("connect")
//                                >> mystring[ref(s1) = _1]
//                                >> lit(",")
//                                >> mystring[ref(s2) = _1]
//                                >> lit(";")
//                            )
//                            ,
//                            space
//                          );
//     if (first != last )
//         return false;
//     return r;
// }
// 
// template <typename Iterator>
// bool new_parser( Iterator first, Iterator last, std::string& s1, std::string& s2) {
// 
// 	//using qi::no_skip;
// 
//     qi::rule<Iterator, std::string(), ascii::space_type> mystring;
//     mystring %= +(char_ - ',' - ';');
// 
//     bool r = phrase_parse( first, last,
//                            (
//                                lit("new")
//                                >> mystring[ref(s1) = _1]
//                                >> lit(",")
//                                >> mystring[ref(s2) = _1]
//                                >> lit(";")
//                            ),
// 							space
//                          );
//     if (first != last )
//         return false;
//     return r;
// }
// 
// template <typename Iterator>
// bool comment_parser( Iterator first, Iterator last)
// {
// 	bool r = phrase_parse( first, last,
// 						   (
// 							   lit("#") >> *( char_)
// 						   ),
// 						space
// 	);
// 	if (first != last )
// 		return false;
// 	return r;
// }

template<class ObjectType>
PipelineLoader<ObjectType>::PipelineLoader(AlgorithmFactory<ObjectType> *factory) : builder(factory)
{
//     this->error = false;
}

template<class ObjectType>
PipelineScheduler<ObjectType>* PipelineLoader<ObjectType>::interpret(std::string& filename)
{
	ConfigParser parser(&builder);
    std::ifstream file(filename.c_str(), std::ios_base::in);
	if (file.good() ) {
		file.unsetf(std::ios::skipws);
		spirit::istream_iterator begin(file);
		spirit::istream_iterator end;
		parser.parseStream(begin,end);
		return builder.getPipelineScheduler();
	} else {
		std::cout << "Error Building the Pipeline: error opening the configuration file";
		return NULL;
	}

}

// template<class ObjectType>
// bool PipelineLoader<ObjectType>::dowork(std::string& line) {
//     std::string s1;
//     std::string s2;
// 	std::string emsg = "";
//     if ( connect_parser<>(line.begin(),line.end(),s1,s2) ) {
// 		if ( tryConnectStage(s1,s2)  ) {
// 			std::cout << "YES "<< s1 << ", " << s2 <<std::endl;
// 			return true;
// 		}
// 		emsg = "while connecting";
//     } else if ( new_parser<>(line.begin(),line.end(),s1,s2) ) {
// 		if ( tryBuildStage(s1,s2) ) {
// 			std::cout << "YES "<< s1 << ", " << s2 <<std::endl;
// 			return true;
// 		}
// 		emsg = "while creating";
// 	} else if ( comment_parser<>(line.begin(),line.end()) ) {
// 		return true;
// 	} else{
// 		emsg = "while parsing";
// 	}
// 	this->error = true;
// 	std::cout << "Error <"<< line << "> " << emsg << std::endl;
// 	return false;
//     
// }
// template<class ObjectType>
// bool PipelineLoader<ObjectType>::tryBuildStage( std::string& type,std::string& name) {
// 	if ( stages[name] == NULL ) {
//                 Stage<ObjectType>* stage = builder.buildStage(type,name);
// 		if ( stage != NULL ) {
// 			stages[name] = stage;
// 			return true;
// 		}
// 	}
// 	return false;
// }
// template<class ObjectType>
// bool PipelineLoader<ObjectType>::tryConnectStage(std::string& s1, std::string& s2)
// {
// 	if ( stages[s1] && stages[s2] ) {
//                 Stage<ObjectType>* _s1 = stages[s1];
//                 Stage<ObjectType>* _s2 = stages[s2];
// 		builder.append(_s1,_s2);
// 		return true;
// 	}
// 	return false;
// }

};

