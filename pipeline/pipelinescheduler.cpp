/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "pipelinescheduler.h"
#include <iostream>
#include <sstream>
#include "queue.h"
namespace ff {

template<class ObjectType>
PipelineScheduler<ObjectType>::PipelineScheduler()
{
	_idleFunction = NULL;
}
template<class ObjectType>
void PipelineScheduler<ObjectType>::shutdown()
{

}
template<class ObjectType>
void PipelineScheduler<ObjectType>::start()
{
	boost::this_thread::yield();
	pool = new ThreadPool(stages.size()+2);
	TStage *mainStage = NULL;
	typename std::list<TStage*>::iterator it = stages.begin();
	while ( it != stages.end() ) {
		TStage * stage = *it;
		if ( stage->algorithm()->type() == MainThread )  {
			mainStage = stage;
			break;
			it++;
		} else if (stage->algorithm()->type() == MultiThread ) {
			pool->assignForever( stage );
			pool->assignForever( stage );
		} else {
			pool->assignForever( stage );
		}
		it++; 
		
	}
	if ( mainStage ) { 
		pool->assignForever(this);
		mainStage->run(NULL);
	} else {
		schedule();
	}

}
template<class ObjectType>
void PipelineScheduler<ObjectType>::schedule()
{
	boost::system_time time;
	
	while ( 1 ) {
		time = boost::get_system_time() + boost::posix_time::milliseconds(2500);
// 		if ( _idleFunction )
// 			_idleFunction();

		boost::this_thread::sleep(time);
		{ 
			std::stringstream buf;
			typename std::list<TStage*>::iterator it = stages.begin();
			while ( it != stages.end() ) {
		
				TStage * stage = *it;
				Queue<ObjectType> *queue = stage->getQueue();
				Delaying &delaying = queue->delaying();
				buf << "\t[" << stage->algorithm()->id() <<"] {"
					<< " AVG@last=" << delaying.getLastAverage() << ", DEL@last=" << delaying.getLastDelay()
					<< ", AVG=" << delaying.getAverage() << ", DEL=" << delaying.getDelay() << std::endl;
				if ( delaying.getDelay() > 0.1 ) {
					if ( stage->algorithm()->type() == MultiThread ) {
						
						if ( pool->hasMoreThreads() ) {
							std::cout << "Scheduling a thread for multithreaded-stage: " << stage->algorithm()->id() << std::endl;
							pool->assignForever(stage);
						} else {
							std::cout << "No more threads to schedule for multithreaded-stage: " << stage->algorithm()->id() << std::endl;
						}
					}
				}
				it++;

			}
			std::cout << "Update: " << std::endl << buf.str();
			std::cout.flush();
		}																																																																													
	}
}

template<class ObjectType>
void PipelineScheduler<ObjectType>::handle(Stage<ObjectType> *stage){
	this->stages.push_back( stage );
}
template<class ObjectType>
PipelineScheduler<ObjectType>::~PipelineScheduler()
{
	while ( !stages.empty() ) {
		TStage * stage = stages.front();
		stages.pop_front();
		delete stage;
	}
}

template<class ObjectType>
void PipelineScheduler<ObjectType>::registerIdleFunction(void (*_idleFunction)(void))
{
	this->_idleFunction = _idleFunction;
}

template<class ObjectType>
void PipelineScheduler<ObjectType>::setup(Thread* thread)
{

}


template<class ObjectType>
void PipelineScheduler<ObjectType>::run(Thread* thread)
{
	this->schedule();
}
};
