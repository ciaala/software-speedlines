/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FF_QUEUEOBJECT_H
#define FF_QUEUEOBJECT_H
#include <time.h>
namespace ff {



template<class ObjectType>
class QueueObject
{
	
private:
	
	ObjectType object;
	
public:
	time_t enque, start, end;
	QueueObject(ObjectType object = NULL);
	
	ObjectType getObject();
	void setObject(ObjectType);
	
	void startTime();
	void endTime();
	void enqueTime();

};

}
#include "queueobject.cpp"
#endif // FF_QUEUEOBJECT_H
