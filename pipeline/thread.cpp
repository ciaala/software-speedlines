/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "thread.h"
#include "boost/thread.hpp"

namespace ff{


Thread::callable::callable(Thread *thread) : run(0), timerunning(0), forever(false), runnable(NULL) {
	this->thread = thread;
}
void Thread::callable::setup(Runnable *runnable, bool forever ) {
	this->forever = forever;
	this->runnable = runnable;
};

void Thread::callable::operator()()
{
	runnable->setup(thread);
	do {
		time_t start;
		time(&start);
		runnable->run(thread);
		time_t end;
		time(&end);
		timerunning += end - start;
	} while( forever == true );
}



Thread::Thread(ThreadPool* pool) : call_it(this)
{
	this->pool = pool;
}

void Thread::executeForever(Runnable* runnable)
{
	call_it.setup(runnable,true);
	boost::thread mythread = boost::thread(call_it);
}
void Thread::executeOnce(Runnable* runnable)
{
	
	call_it.setup(runnable,false);
	boost::thread mythread = boost::thread(call_it);
}




};

