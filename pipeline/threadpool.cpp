/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "threadpool.h"

namespace ff { 

ThreadPool::ThreadPool(int count, int threshold, int max)
{
	this->count = count;
	this->threshold = threshold;
	this->max = max;
	
	for( int i=0; i<count; i++ ){
		Thread *thread = new Thread(this);
		free.push_back(thread);
	}
}
ThreadPool::~ThreadPool( )
{
	while( free.empty() ) {
		Thread *t = free.front();
		free.pop_front();
		delete t;
	}
	while( assigned.empty() ) {
		Thread *t = assigned.front();
		assigned.pop_front();
		delete t;
	}
	while( forever.empty() ) {
		Thread *t = forever.front();
		forever.pop_front();
		delete t;
	}
}

void ThreadPool::assignForever(Runnable* runnable)
{

	if ( hasMoreThreads() ) {
		Thread* thread = free.front();
		free.pop_front();
		forever.push_front(thread);
		thread->executeForever(runnable);
	}
}

void ThreadPool::assignOnce(Runnable* runnable)
{
	if ( hasMoreThreads() ) {
		Thread* thread = free.front();
		free.pop_front();
		forever.push_front(thread);
		thread->executeForever(runnable);
	}
}

void ThreadPool::poolcheck()
{
	if ( free.size() == 0 && max > count) {
		count += threshold;
		for( int i=0; i < threshold; i++ ) {
			Thread* thread = new Thread(this);
			free.push_back(thread);
		}
	}
}
bool ThreadPool::hasMoreThreads()
{
	poolcheck();
	return	free.size() > 0;
}

}