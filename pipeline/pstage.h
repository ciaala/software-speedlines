#ifndef FF_PSTAGE_H
#define FF_PSTAGE_H

#include <string>
#include <map>
#include <boost/variant.hpp>

typedef boost::variant< std::string, double > value_type;
typedef std::map<std::string, value_type> map_v_type;
typedef std::pair<std::string, value_type> pair_v_type;
typedef map_v_type::iterator map_v_type_it;

namespace ff {
	
struct pstage
{
	std::string type;
	std::string name;
	map_v_type paramv;
};

};

#endif
	