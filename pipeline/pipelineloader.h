/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FF_PIPELINELOADER_H
#define FF_PIPELINELOADER_H

#include <list>
#include <string>
#include <map>

#include "algorithmfactory.h"
#include "pipelinebuilder.h"
#include "pipelinescheduler.h"

namespace ff {
	

template<class ObjectType>
class PipelineLoader
{
    
	
private:
    PipelineBuilder<ObjectType> builder;


// 	bool error;
	
// 	bool dowork(std::string& line);
// 	bool tryBuildStage( std::string& type,std::string& name );
// 	bool tryConnectStage( std::string& s1, std::string& s2);
public:
//     PipelineLoader(PipelineBuilder<ObjectType> *builder);
	PipelineLoader(AlgorithmFactory<ObjectType> *factory);
// 	~PipelineLoader();
    PipelineScheduler<ObjectType>* interpret(std::string& filename);

};

}
#include "pipelineloader.cpp"
#endif // FF_PIPELINELOADER_H
