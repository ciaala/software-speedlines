/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "delaying.h"
#include "math.h"
#include <algorithm>
#include <boost/thread/locks.hpp>

namespace ff {

class Sum {
private :
	double value;
public :
	Sum(double initial = 0) : value(initial) { }
	void operator() (double x) { value += x; }
	double result() const { return value; }
};

Delaying::Delaying(int memory)
{
	this->delay = 0;
	this->average = 0;
	this->memory = memory;
	this->counter = 0;

	//this->last_averages = 0;
	//this->last_delays = 0;
}

void Delaying::update(long int enqueue, long int start, long int end)
{
	
	boost::lock_guard<boost::mutex> lock(_mutex);
	double new_d = end - enqueue;
	double new_avg = end - start;
	if ( last_delays.size() > memory ) {
		last_delays.pop_back();
	}
	last_delays.push_front(new_d);

	if ( last_averages.size() > memory ) {
		last_averages.pop_back();
	}
	last_averages.push_front(new_avg);
	average += new_avg;
	delay += new_d;
	counter++;
}

double Delaying::getAverage()
{
	boost::lock_guard<boost::mutex> lock(_mutex);
	return average/counter;
}
double Delaying::getLastAverage()
{
	boost::lock_guard<boost::mutex> lock(_mutex);
	if ( counter > 0 ) {
		Sum s;
		s = std::for_each(last_averages.begin(), last_averages.end(), s);
		return s.result()/memory;
	}
	return INFINITY;
}

double Delaying::getDelay()
{
	boost::lock_guard<boost::mutex> lock(_mutex);
	return delay/counter;
}
double Delaying::getLastDelay()
{
	boost::lock_guard<boost::mutex> lock(_mutex);
	if ( counter > 0 ) {
		Sum s;
		s = std::for_each(last_delays.begin(), last_delays.end(), s);
		return s.result()/memory;
	} 
	return INFINITY;
}


};