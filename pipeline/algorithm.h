/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FF_ALGORITHM_H
#define FF_ALGORITHM_H
#include <string>
#include "parameters.h"

namespace ff {




enum AlgorithmRole { Sink, Source, Link };
enum AlgorithmType { SingleThread, MultiThread, MainThread };

template<class ObjectType>
class Algorithm
{


protected:
    AlgorithmRole 	_role;
    AlgorithmType	_type;
	std::string		_id;
	Parameters 		_params;
private :
    Algorithm();

public:

    Algorithm(std::string &id, AlgorithmRole role, AlgorithmType type);
    ~Algorithm();
	
	virtual bool doSetup();
    virtual ObjectType doWork(ObjectType in) = 0;

    /** */
    AlgorithmRole role();
    /** */
    AlgorithmType type();

	//void setID(std::string &id);
	std::string id();

	Parameters* parameters();
};

}

#include "algorithm.cpp"

#endif // FF_ALGORITHM_H
