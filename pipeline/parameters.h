/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FF_PARAMETERS_H
#define FF_PARAMETERS_H
#include <map>
#include <boost/thread.hpp>
namespace ff {

class Parameters
{
	boost::mutex _mutex;
	bool _changed;
	std::map<std::string, double> _doubles;
	std::map<std::string, std::string> _strings;
protected:
// 	void registerDouble(std::string name, double *container );
// 	void registerString(std::string name, std::string *container );
public:
	Parameters();
	
	void setString(std::string &name, std::string &value);
	void setDouble(std::string &name, double value);

	
	double getDouble(std::string &name);
	std::string& getString(std::string &name);
	
	void lock();
	void unlock();
	bool changed();
	void setUnchanged();
};

}

#endif // FF_PARAMETERS_H
