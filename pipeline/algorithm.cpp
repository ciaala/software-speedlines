/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "algorithm.h"

namespace ff {


template<class ObjectType>
Algorithm<ObjectType>::Algorithm(std::string &id, AlgorithmRole role, AlgorithmType type)
{
	this->_role = role;
	this->_type = type;
	this->_id = id;
}
template<class ObjectType>
AlgorithmRole Algorithm<ObjectType>::role()
{
	return _role;
}

template<class ObjectType>
AlgorithmType Algorithm<ObjectType>::type()
{
	return _type;
}

template<class ObjectType>
Algorithm<ObjectType>::Algorithm() { }

template<class ObjectType>
Algorithm<ObjectType>::~Algorithm() { }

/*
template<class ObjectType>
void Algorithm<ObjectType>::setID(std::string &id)
{
	this->_id = id;
}
*/

template<class ObjectType>
std::string Algorithm<ObjectType>::id()
{
	return this->_id;
}

template<class ObjectType>
bool Algorithm<ObjectType>::doSetup() { return true; }


template<class ObjectType>
Parameters* Algorithm<ObjectType>::parameters() {
	return &(this->_params);
}


}; // end of Namespace
