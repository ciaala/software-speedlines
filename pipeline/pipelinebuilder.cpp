/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "pipelinebuilder.h"
#include "pstage.h"
#include "boost/variant.hpp"

namespace ff {
template<class ObjectType>
PipelineBuilder<ObjectType>::PipelineBuilder(AlgorithmFactory<ObjectType> *factory)
{
    this->factory = factory;

}

// template<class ObjectType>
// Stage<ObjectType>* PipelineBuilder<ObjectType>::buildStage(std::string &type, std::string &id)
// {
//     //Algorithm<ObjectType> *algorithm =
//     //algorithm->setName(name);
//     Stage<ObjectType> *stage =  factory->build(type,id);
//     //new Stage<ObjectType>(algorithm);
//     scheduler.handle(stage);
//     return stage;
// }
/*
template<class ObjectType>
void PipelineBuilder<ObjectType>::append(Stage<ObjectType>* before, Stage<ObjectType>* after)
{
    before->appendStage(after);
}*/
template<class ObjectType>
PipelineScheduler<ObjectType>* PipelineBuilder<ObjectType>::getPipelineScheduler()
{
    return &scheduler;
}





template<class ObjectType>
void PipelineBuilder<ObjectType>::__connect(std::list< std::string >& chain)
{
    std::cout << "-------------------------------- \n";
    std::cout << "PipelineBuilder [Connect]\n";
    std::list<std::string>::iterator it = chain.begin();
    std::list<std::string>::iterator end = chain.end();
	
	Stage<ObjectType> *step = stages[(*it)];
	std::cout << (*it);
	++it;
    do {
		Stage<ObjectType> *current = stages[(*it)];
		step->appendStage(current);
		std::cout <<" >> " << (*it);
		step = current;
		++it;
	} while ( it != end);
	std::cout << std::endl;
    std::cout << "---------------------------------" << std::endl;
}

template<class ObjectType>
void PipelineBuilder<ObjectType>::__stage(pstage& _stage)
{
	std::cout << "-------------------------------- \n";
	std::cout << "PipelineBuilder [Stage] \n" <<_stage.type << " "<< _stage.name <<std::endl;
	if ( stages[_stage.name] == NULL ) {
		Stage<ObjectType> *stage =  factory->build(_stage.type,_stage.name);

		if ( stage ) {
			this->stages[_stage.name] = stage;
			current_stage = stage;
			scheduler.handle(stage);
			map_v_type &m = _stage.paramv;
			map_v_type::iterator end = m.end();
			for (map_v_type::iterator it = m.begin(); it != end; ++it)
			{
				pair_v_type pair = (*it);
				current_key = &(pair.first);

				value_type second = pair.second;
				if (!second.empty()) {
					second.apply_visitor<>(*this);
				} else {
					std::cout << *current_key << std::endl;
				}
			}
		} else {
			std::cerr << "Unable to build stage: "<< _stage.name << " of type " << _stage.type << std::endl;
		}
	} else {
		std::cerr << "A stag with identifier: "<< _stage.name << " has already been created" << std::endl;
	}
}


template<class ObjectType>
void PipelineBuilder<ObjectType>::operator()(double value)
{
    std::cout << *current_key << " = " << value <<std::endl;
	current_stage->parameters()->setDouble(*current_key, value);
}

template<class ObjectType>
void PipelineBuilder<ObjectType>::operator()(std::string value)
{
    std::cout << *current_key << " = " << value << std::endl;
	current_stage->parameters()->setString(*current_key, value);
	
}





};
