/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "stage.h"




namespace ff {
template<class ObjectType>
Stage<ObjectType>::Stage(Algorithm<ObjectType>* algorithm)
{
	this->_algorithm = algorithm;
}
template<class ObjectType>
void Stage<ObjectType>::shutdown()
{

}


template<class ObjectType>
void Stage<ObjectType>::appendObject(QueueObject<ObjectType>* object)
{
	this->queue.push_back(object);
}

template<class ObjectType>
bool Stage<ObjectType>::appendStage(TStage *next)
{

	if ( nexts.find( next ) == nexts.end()) {
		nexts.insert(next);
		return true;
	}
	return false;
}

template<class ObjectType>
void Stage<ObjectType>::setup(Thread* runner)
{
//	if ( runner )
//		runners.push_back(runner);
	boost::lock_guard<boost::mutex> lock(_mutex);
	_algorithm->doSetup();
//	if ( runner )
//		runners.remove(runner);
}

template<class ObjectType>
void Stage<ObjectType>::run(Thread* runner)
{
	
	//if ( runner )
	//	runners.push_back(runner);
	
	//std::cout << "ALG[IN]: "<< algorithm->name() << std::endl;
	ObjectType o = NULL;
	QueueObject<ObjectType> * q_o = NULL;
	if ( _algorithm->role() != Source ) {
		q_o = queue.pop_front();
		o = q_o->getObject();
	} else {
		q_o = new QueueObject<ObjectType>(NULL);
		o = NULL;
	}
	// if algorithm is of type source it is ok to call with a null doWork //
		
	ObjectType out = _algorithm->doWork( o );

	
	q_o->endTime();
	Delaying &delaying = queue.delaying();
	delaying.update(q_o->enque,q_o->start,q_o->end);
	if ( out && _algorithm->role() != Sink ) {
		propagate(out,q_o);
	} else if( _algorithm->role() == Sink && q_o) {
		delete q_o;
	}

	//std::cout << "ALG[OUT]: "<< algorithm->name() << std::endl;
//	if ( runner )
//		runners.remove(runner);
}


template<class ObjectType>
void Stage<ObjectType>::propagate(ObjectType object, QueueObject<ObjectType>* oldQO)
{
	oldQO->setObject(object);
	QueueObject<ObjectType>* q_o = oldQO;
	typename std::set <TStage *>::iterator it = nexts.begin();
	
	while ( it != nexts.end() ) {
		Stage<ObjectType> *stage = *it;
		// Prepare a new queueobject container for the pipelineobject
		if ( q_o == NULL ) {
			q_o = new QueueObject<ObjectType>();
			*q_o = *oldQO;
		}
		stage->appendObject(q_o);
		it++;
		// trigger the creation of a new queueobject when we have multiple stage to feed
		q_o = NULL;
	}
}

template<class ObjectType>
Queue<ObjectType>* Stage<ObjectType>::getQueue()
{
	return &queue;
}

template<class ObjectType>
Algorithm<ObjectType>* Stage<ObjectType>::algorithm()
{
	return _algorithm;
}


template<class ObjectType>
Parameters* Stage<ObjectType>::parameters()
{
	return _algorithm->parameters();
}

}
