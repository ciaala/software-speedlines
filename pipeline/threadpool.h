/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FF_THREADPOOL_H
#define FF_THREADPOOL_H

#include <list>
#include "thread.h"

namespace ff {
class Thread;
class Runnable;

class ThreadPool
{
private:
	int count;
	int threshold;
	int max;
private:
	std::list<Thread*> forever;
	std::list<Thread*> assigned;
	std::list<Thread*> free;

	void poolcheck( );
public:
    ThreadPool(int count = 4, int threshold = 2, int max = 12);
	~ThreadPool();
	void assignForever(Runnable* runnable);
	void assignOnce(Runnable* runnable);
    bool hasMoreThreads();
};

}

#endif // FF_THREADPOOL_H
