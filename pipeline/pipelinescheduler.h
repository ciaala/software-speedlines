/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FF_PIPELINESCHEDULER_H
#define FF_PIPELINESCHEDULER_H
#include <list>

#include "stage.h"
#include "threadpool.h"

namespace ff {

template<class ObjectType>
class PipelineScheduler : public ff::Runnable
{
	typedef Stage<ObjectType> TStage;
private:
    std::list<TStage*> stages;
    ThreadPool *pool;
	void (*_idleFunction)(void);
public:
    PipelineScheduler();
	~PipelineScheduler();
	void start();
	void registerIdleFunction(void (*idleFunction)(void));
    void shutdown();
	
    void handle(Stage<ObjectType>* stage);
    void schedule();
	virtual void setup(Thread* runner);
    virtual void run(Thread* runner);
};

}
#include "pipelinescheduler.cpp"
#endif // FF_PIPELINESCHEDULER_H
