/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "queueobject.h"
#include <math.h>
namespace ff {
template<class ObjectType>
QueueObject<ObjectType>::QueueObject(ObjectType object)
{
	this->start= -1;
	this->end = -1;
	this->enque = -1;
	this->object = object;
}

template<class ObjectType>
ObjectType QueueObject<ObjectType>::getObject()
{
	return object;
}

template<class ObjectType>
void QueueObject<ObjectType>::setObject(ObjectType object )
{
	this->object = object;
}

template<class ObjectType>
void ff::QueueObject<ObjectType>::endTime()
{
	time( &end );
}

template<class ObjectType>
void ff::QueueObject<ObjectType>::startTime()
{
	time( &start );
}

template<class ObjectType>
void ff::QueueObject<ObjectType>::enqueTime()
{
	time( &enque );
}

};
