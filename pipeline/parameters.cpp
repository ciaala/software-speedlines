/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "parameters.h"

namespace ff{

Parameters::Parameters()
{
	_changed = false;
}

double Parameters::getDouble(std::string &name)
{
	return _doubles[name];
}

std::string& Parameters::getString(std::string &name)
{
	return _strings[name];
}
void Parameters::lock()
{
	this->_mutex.lock();
}
void Parameters::unlock()
{
	this->_mutex.unlock();
}
bool Parameters::changed()
{
	return this->_changed;
}
void Parameters::setUnchanged()
{
	this->_changed = false;
}

void Parameters::setDouble(std::string &name, double value)
{
	_doubles[name] = value;
	_changed = true;
}

void Parameters::setString(std::string &name, std::string &value)
{
	_strings[name] = value;
	_changed = true;
}

};