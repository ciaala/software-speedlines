#include "parser.h"
#include <boost/spirit/include/qi_parse.hpp>
namespace ff {
	

ConfigParser::ConfigParser (ConfigBuilder* builder): ConfigParser::base_type(start)
{
	skip = *(ascii::blank | qi::eol);
	sstring %= skip >> ascii::char_("a-zA-Z_") >> *ascii::char_("a-zA-Z_0-9");
	qstring %= skip >> qi::lit('"') >> qi::lexeme[*(ascii::char_ - '"')] >> qi::lit('"');
	
	comment %= (qi::lit("/*") >> *(ascii::char_ - "*/") >> qi::lit("*/"))
				| ( qi::lit('#') >> *(ascii::char_ - '\n') > qi::lit('\n'))
				| (qi::lit("//") >> *(ascii::char_ - '\n') > qi::lit('\n'));
	
	
	connect %= sstring % ( skip >> qi::lit(">>")) >> skip >>qi::lit(';')[phoenix::bind(&ConfigBuilder::__connect,builder, qi::_val)];

	value = (qstring | skip >> qi::double_  );
	pairv %= (sstring|qstring) >> skip >> -( qi::lit('=') >> value ) ;
	paramv %= pairv % qi::lit(',');
	stage %= sstring >> sstring >> -(skip >> qi::lit('{') >> paramv >> skip >> -(qi::lit(',') >> skip) >> qi::lit('}') )
			>> skip	>> qi::lit(';')[phoenix::bind(&ConfigBuilder::__stage, builder,qi::_val)];

	start = +( +ascii::blank | +qi::eol |qi::lit("stage ") >> stage | qi::lit("connect ") >> connect | comment);
	
#ifdef DEBUG_SPIRIT_DEBUG
	BOOST_SPIRIT_DEBUG_NODE(start);
	BOOST_SPIRIT_DEBUG_NODE(stage);
	BOOST_SPIRIT_DEBUG_NODE(comment);
	BOOST_SPIRIT_DEBUG_NODE(connect);
	BOOST_SPIRIT_DEBUG_NODE(sstring);
	BOOST_SPIRIT_DEBUG_NODE(qstring);
	BOOST_SPIRIT_DEBUG_NODE(skip);
	BOOST_SPIRIT_DEBUG_NODE(pairv);
	BOOST_SPIRIT_DEBUG_NODE(paramv);
	BOOST_SPIRIT_DEBUG_NODE(value);
#endif
}


bool ConfigParser::parseStream(spirit::istream_iterator &begin, spirit::istream_iterator &end)
{
	//using boost::spirit::qi::parse;
	bool result = qi::parse(begin, end,*this);
	return result;
}
	
};