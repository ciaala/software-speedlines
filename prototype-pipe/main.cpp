/*
 *    <one line to give the program's name and a brief idea of what it does.>
 *    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>
 * 
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 * 
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 * 
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../pipeline/pipeline.h"
#include "prototypefactory.h"
#include "prototypeobject.h"
using namespace ff;

void cvIdle() { cv::waitKey(1); }

int main(int argc, char** argv) {
	PrototypeFactory factory;

	ff::PipelineLoader<PrototypeObject*> loader( &factory );
	std::string filename = "/home/crypt/projects/software_speedlines/prototype.cfg";
	if (argc == 2 ) {
		filename = argv[1];
	}
	std::cout.flush();
	ff::PipelineScheduler<PrototypeObject*> *scheduler = loader.interpret(filename);
	if (scheduler) {
		//scheduler->registerIdleFunction(cvIdle);
		scheduler->start();
		return 0;
	} else {
		std::cout << std::endl << "Unable to create the pipeline" << std::endl;
		return -1;
	}
}