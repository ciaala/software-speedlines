/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "prototypefactory.h"
#include "../opencv-block/opencv-block.h"

namespace ff {

Stage<PrototypeObject*>* PrototypeFactory::build(std::string& type, std::string &id)
{
	ff::Algorithm<Frame*> *algorithm = NULL;

	if ( type.compare("CameraCapture") == 0 ) {
		algorithm = new CameraCapture(id);
	} else if ( type.compare("VideoCapture") == 0 ) {
		algorithm = new VideoCapture(id);
	} else if ( type.compare("FrameWindow") == 0 ) {
		algorithm = new FrameWindow(id);
	} else if ( type.compare("SecondaryWindow") == 0 ) {
		algorithm = new SecondaryWindow(id);
	} else if ( type.compare("Background") == 0 ) {
		algorithm = new Background(id);
	} else {
		return NULL;
	}
 	return new Stage<Frame*>(algorithm);

}



};

