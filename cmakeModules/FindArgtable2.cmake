if(WIN32)
  
	STRING(REGEX REPLACE ".*;([^;]+)argtable2([^;]+)[;$]" "\\1argtable2\\2" ARGTABLE_ROOT "$ENV{Path}" )
 	
	find_path(ARGTABLE2_INCLUDE_DIR argtable2.h PATH ${ARGTABLE_ROOT} ${ARGTABLE_ROOT}/include ${ARGTABLE_ROOT}/src)
	find_library(ARGTABLE2_LIBRARY argtable2.lib PATH ${ARGTABLE_ROOT} ${ARGTABLE_ROOT}/lib ${ARGTABLE_ROOT}/src)
else(WIN32)
	find_path(ARGTABLE2_INCLUDE_DIR argtable2.h /usr/include/ /usr/local/include/ /opt/include /opt/local/include)
	find_library(ARGTABLE2_LIBRARY NAMES argtable2 PATH /usr/lib /usr/local/lib /opt/lib /opt/local/lib)
endif(WIN32)

if (ARGTABLE2_INCLUDE_DIR AND ARGTABLE2_LIBRARY)
	message(STATUS "Found Argtable2: ${ARGTABLE2_LIBRARY} ${ARGTABLE2_INCLUDE_DIR}")
else (ARGTABLE2_INCLUDE_DIR AND ARGTABLE2_LIBRARY)
	message(FATAL_ERROR "Could not find Argtable2")
endif (ARGTABLE2_INCLUDE_DIR AND ARGTABLE2_LIBRARY)
	
MARK_AS_ADVANCED(
  	ARGTABLE2_INCLUDE_DIR
  	ARGTABLE2_LIBRARY
)
