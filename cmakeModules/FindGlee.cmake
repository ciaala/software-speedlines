	STRING(REGEX REPLACE ".*;([^;]+)glee([^;]+)[;$]" "\\1glee\\2" GLEE_ROOT "$ENV{Path}" )
	find_library(GLEE_LIBRARY GLee.lib PATH ${GLEE_ROOT} ${GLEE_ROOT}/lib ${GLEE_ROOT}/src)

if (GLEE_LIBRARY)
	message(STATUS "Found GLEE: ${GLEE_LIBRARY}")
else (GLEE_LIBRARY)
	message(FATAL_ERROR "Could not find GLEE")
endif (GLEE_LIBRARY)
	
MARK_AS_ADVANCED(
  	GLEE_LIBRARY
)