
/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "videocapture.h"

#include <opencv2/core/core.hpp>

using namespace ff;

VideoCapture::VideoCapture(std::string &id) : Algorithm< ff::Frame* >(id, Source, SingleThread), _capture()
{

}

bool VideoCapture::doSetup(){
	std::string key("filename");
	std::string &filename = _params.getString(key);
	this->_capture.open(filename);
	
	if ( this->_capture.isOpened() ) {
		std::cout << "video stream from file, filename :" << filename << "opened" << std::endl;
		return true;
	} else {
		std::cerr << "It wasn't possible to open the file: "<< filename << std::endl;
		return false;
	}
}

Frame* VideoCapture::doWork(Frame* in)
{

	Frame *frame = new Frame();
	cv::Mat grabbed;
	do { 
	_capture >> grabbed;
	} while ( grabbed.empty() );
	frame->captured = grabbed.clone();
	frame->selected = &frame->captured;
	//usleep( 33*1000);
	return frame;
}
