#include "utils.hpp"


BWPixel median(std::set<BWPixel>& v){
	int k;
	if ( ( v.size() % 2 ) == 1 ) k=v.size()/2;
	else k=v.size()/2 -1;

	std::set<BWPixel>::iterator iter=v.begin();
	for(int i=0; i<k-1; iter++, i++ );
	return *iter;
}
BWPixel mean(std::vector<BWPixel>& v){
	int mean=0;
	for(int i=0; i<v.size(); i++)
		mean+=v[i];
	mean/=v.size();
	return mean;
}


bool cmpBWPixel(BWPixel i, BWPixel j){
	return ( i < j );
}


// int distanceColor(RGBPixel* px1, RGBPixel* px2){
// 	BWPixel diffR = abs( (int)(px1->R) - (int)(px2->R) );
// 	BWPixel diffG = abs( (int)(px1->G) - (int)(px2->G) );
// 	BWPixel diffB = abs( (int)(px1->B) - (int)(px2->B) );
// 	return diffR+diffG+diffB;
// }


void sumSpeedlinesWithFading(cv::Mat& precSpeedlines, cv::Mat& currentSpeedlines, int fadeStep){
	BWPixel* bw;
	for ( register int x = 0; x < currentSpeedlines.cols; x++ ){
		for ( register int y = 0; y < currentSpeedlines.rows; y++ ){
			bw= &MAT_ELEM(precSpeedlines, BWPixel, y, x);

			if ( MAT_ELEM(currentSpeedlines, BWPixel, y, x) != BW_BLACK)
				*bw = BW_WHITE;
			else if(*bw!=BW_BLACK){
				*bw= *bw<=fadeStep ? BW_BLACK : *bw - fadeStep;
			}
		}
	}
}

cv::Mat extractBackgroundMedian(std::vector<cv::Mat> frames){
	cv::Mat				background;
	std::set<BWPixel>	arrayR;
	std::set<BWPixel>	arrayG;
	std::set<BWPixel>	arrayB;
	int						width;
	int						height;
	int						type;
	RGBPixel* 				pixelFRM;
	RGBPixel* 				pixelBG;

	width = frames[0].cols;
	height = frames[0].rows;
	type = frames[0].type();
	background = cv::Mat(height, width, type);

	for ( int x = 0; x < width; x++ )
	{
		for ( int y = 0; y < height; y++ )
		{
			arrayR.clear();
			arrayG.clear();
			arrayB.clear();
			for ( int i = 0; i < frames.size(); i++ )
			{
				pixelFRM= &MAT_ELEM(frames[i], RGBPixel, y, x);
				arrayR.insert(pixelFRM->R);
				arrayG.insert(pixelFRM->G);
				arrayB.insert(pixelFRM->B);
			}

			pixelBG= &MAT_ELEM(background, RGBPixel, y, x);
			pixelBG->R = median(arrayR);
			pixelBG->G = median(arrayG);
			pixelBG->B = median(arrayB);
		}
	}
	return background;
}

cv::Mat extractBackgroundMean(std::vector<cv::Mat> frames){
	cv::Mat				background;
	std::vector<BWPixel>	arrayR;
	std::vector<BWPixel>	arrayG;
	std::vector<BWPixel>	arrayB;
	int						width;
	int						height;
	int						type;
	RGBPixel* 				pixelFRM;
	RGBPixel* 				pixelBG;

	width = frames[0].cols;
	height = frames[0].rows;
	type = frames[0].type();
	background = cv::Mat(height, width, type);

	for ( int x = 0; x < width; x++ ){
		for ( int y = 0; y < height; y++ ){
			arrayR.clear();
			arrayG.clear();
			arrayB.clear();
			for ( int i = 0; i < frames.size(); i++ ){
				pixelFRM= &MAT_ELEM(frames[i], RGBPixel, y, x);
				arrayR.push_back(pixelFRM->R);
				arrayG.push_back(pixelFRM->G);
				arrayB.push_back(pixelFRM->B);
			}

			pixelBG= &MAT_ELEM(background, RGBPixel, y, x);
			pixelBG->R = mean(arrayR);
			pixelBG->G = mean(arrayG);
			pixelBG->B = mean(arrayB);
		}
	}
	return background;
}
