
#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <vector>
#include <set>
#include <highgui.h>

typedef unsigned char BWPixel;

typedef struct{
	unsigned char B;
	unsigned char G;
	unsigned char R;
} RGBPixel;

#define MAT_ELEM_PTR_FAST( mat, row, col, pix_size )  \
    (assert( (unsigned)(row) < (unsigned)(mat).rows &&   \
             (unsigned)(col) < (unsigned)(mat).cols ),   \
     (mat).data + (size_t)(mat).step*(row) + (pix_size)*(col))

#define MAT_ELEM_PTR( mat, row, col )                 \
    MAT_ELEM_PTR_FAST( mat, row, col, CV_ELEM_SIZE((mat).type) )

#define MAT_ELEM( mat, elemtype, row, col )           \
    (*(elemtype*)MAT_ELEM_PTR_FAST( mat, row, col, sizeof(elemtype)))

#define MIN_UTILS(x, y)			\
	((x)>(y)?y:x)
#define MAX_UTILS(x,y)			\
	((x)>(y)?x:y)

#define DISTANCE_COLOR(image, background)		\
	MAX_UTILS((image).B, (background).B)-MIN_UTILS((image).B, (background).B) + \
	MAX_UTILS((image).G, (background).G)-MIN_UTILS((image).G, (background).G) + \
	MAX_UTILS((image).R, (background).R)-MIN_UTILS((image).R, (background).R)


#define BW_WHITE 255
#define BW_BLACK 0

BWPixel median(std::set<BWPixel>& v);
BWPixel mean(std::vector<BWPixel>& v);
bool cmpBWPixel(BWPixel i, BWPixel j);
// int distanceColor(RGBPixel* px1, RGBPixel* px2);

inline int distanceColor(RGBPixel* px1, RGBPixel* px2){
	BWPixel diffR = abs( (int)(px1->R) - (int)(px2->R) );
	BWPixel diffG = abs( (int)(px1->G) - (int)(px2->G) );
	BWPixel diffB = abs( (int)(px1->B) - (int)(px2->B) );
	return diffR+diffG+diffB;
}



void sumSpeedlinesWithFading(cv::Mat& precSpeedlines, cv::Mat& currentSpeedlines, int fadeStep);

cv::Mat extractBackgroundMean(std::vector<cv::Mat> frames);
cv::Mat extractBackgroundMedian(std::vector<cv::Mat> frames);


#endif
