/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "framewindow.h"

namespace ff {

FrameWindow::FrameWindow(std::string &id) : Algorithm< ff::Frame* >(id,Sink,SingleThread)
{
	
//	cv::namedWindow(_id.c_str(),CV_GUI_NORMAL ||  CV_WINDOW_AUTOSIZE);
	_setuped = false;
	
	//cv::w
	//cv::namedWindow(_id.c_str(),CV_GUI_EXPANDED ||  CV_WINDOW_AUTOSIZE);
}
bool FrameWindow::doSetup() {

//	cv::namedWindow("w2",CV_GUI_NORMAL ||  CV_WINDOW_AUTOSIZE);
//	cv::startWindowThread();
	cv::namedWindow(_id.c_str(), CV_WINDOW_NORMAL);
//	cv::namedWindow(_id.c_str(),CV_GUI_EXPANDED ||  CV_WINDOW_AUTOSIZE);
	return true;
}

Frame* FrameWindow::doWork(Frame* in)
{
// 	if ( !_setuped ) {
// 		doSetup();
// 		_setuped = true;
// 	}
//	int longo = 4;
// 	if ( _setup[in->window] == false ) {
// 			_setup[in->window] = true;
//  			cv::namedWindow(in->window.c_str(),CV_GUI_EXPANDED ||  CV_WINDOW_AUTOSIZE);
//  			std::cout << "window[" << in->window << "] created !" << std::endl;
// 	}
	
	if ( in ) {
		if ( (in->sid() %100 )== 0 ) {
			std::cout << "frame[" <<in->sid() << "]@" <<in->window << std::endl;
			std::cout.flush();
		}
		cv::imshow(_id.c_str(), *(in->selected) );
 	//	cv::imshow(in->window.c_str(), *(in->selected) );
	//	cv::imshow("w2", in->captured );
 		cv::waitKey(1);
		//delete in;
	}
}

};
