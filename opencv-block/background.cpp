/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include "background.h"
#include "../src/utils.hpp"
namespace ff {

Background::Background(string& id): Algorithm< ff::Frame* >(id,Link,MultiThread)
{
    this->_memory = 10;
}

Frame* Background::doWork(Frame* in)
{
	std::vector<cv::Mat> temp;
	Frame *out;
	{
		boost::unique_lock<boost::mutex> lock(multi);
		stack.push_back(in);
		frames.push_back(in->captured);
	
		if ( stack.size() < _memory ) {
			return NULL;
		}

		out = stack.front();
		list< cv::Mat >::iterator begin = frames.begin();
		list< cv::Mat >::iterator end =frames.end();
		temp = std::vector<cv::Mat>(begin,end);
		stack.pop_front();
		frames.pop_front();
	}
	
	out->background = extractBackgroundMedian(temp);
	out->selected = &out->background;
	return out;
    
    
}

}

