/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "secondarywindow.h"

namespace ff {

SecondaryWindow::SecondaryWindow(std::string &id) : Algorithm< ff::Frame* >(id,Sink,SingleThread)
{
	
	_setuped = false;
	//cv::w
	//cv::namedWindow(_id.c_str(),CV_GUI_EXPANDED ||  CV_WINDOW_AUTOSIZE);
//	std::cout << "window[" << _id << "] created !" << std::endl;
	cv::namedWindow(_id.c_str(),CV_GUI_EXPANDED ||  CV_WINDOW_AUTOSIZE);
}
bool SecondaryWindow::doSetup() {
	
	
	cv::startWindowThread();
	return true;
}

Frame* SecondaryWindow::doWork(Frame* in)
{
	if ( !_setuped ) {
		doSetup();
		_setuped = true;
	}
	if ( in )  {
//		std::cout << "window["<< _id << "]frame[" <<in->sid() << "]" << std::endl;
//		std::cout.flush();
//		cv::imshow(_id.c_str(), *(in->selected) );
//		cv::waitKey(1);
		//delete in;
// 		in->window = this->_id;
	}
	return in;
}

};