/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "../pipeline/algorithm.h"
#include "frame.h"

#ifndef FF_SECONDARYWINDOW_H
#define FF_SECONDARYWINDOW_H

namespace ff {

class SecondaryWindow  : public Algorithm<Frame*>
{
private:
	bool _setuped;
public:
	SecondaryWindow(std::string &id);
	virtual Frame* doWork(Frame* in);
	virtual bool doSetup();
};

}

#endif // FF_SECONDARYWINDOW_H
