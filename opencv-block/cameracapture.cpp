/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "cameracapture.h"


namespace ff { 

CameraCapture::CameraCapture(std::string &id) : Algorithm< ff::Frame* >(id, Source, SingleThread)
{
	this->_capture.open(CV_CAP_ANY);

	std::cout << this->_capture.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
	std::cout << this->_capture.set(CV_CAP_PROP_FRAME_WIDTH, 640);

//	std::cout << this->_capture.get(CV_CAP_PROP_FPS);
}

Frame* CameraCapture::doWork(ff::Frame* in)
{
	Frame *frame = new Frame();
	do {
		_capture >> frame->captured;
	} while ( frame->captured.empty() );
	//std::cout << this->_capture.get(CV_CAP_PROP_FPS);
	return frame;
}

};