/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "myfactory.h"
#include <iostream>

namespace toy {

using namespace ff;
	
class ToyAl1 : public Algorithm<MyObject*> {
public:
	ToyAl1(std::string &id) : Algorithm< toy::MyObject* >(id,Source, SingleThread){}
    virtual MyObject* doWork(MyObject* in) {
		return new MyObject();
    }
};

class ToyAl2 : public Algorithm<MyObject*> {
public:
	ToyAl2(std::string &id) : Algorithm< toy::MyObject* >(id,Link, MultiThread) {}
    virtual MyObject* doWork(MyObject* in) {
		in->setValue(in->getValue()/2.0);
		return in;
    }
};

class ToyAl3 : public Algorithm<MyObject*> {
public:
	ToyAl3(std::string &id) : Algorithm< toy::MyObject* >(id,Link, MultiThread) {}
	virtual MyObject* doWork(MyObject* in) {

		in->setValue(in->getValue()*in->getValue());
		return in;
	}
};
class ToyAl4 : public Algorithm<MyObject*> {
public:
	ToyAl4(std::string &id) : Algorithm< toy::MyObject* >(id,Sink, SingleThread) {}
	virtual MyObject* doWork(MyObject* in) {
		
		std::cout << "done] " << in->getValue() << std::endl;
		
		// We can remove it here because we are in the semantic of the application, we know nobody needs it anymore
		delete in;
		return NULL;
	}
};

ff::Algorithm<MyObject*>* MyFactory::build(std::string& type,std::string &id)
{
	ff::Algorithm<MyObject*> *algorithm;
	if (type.compare("Toy1") == 0) {
		algorithm = new ToyAl1(id);
	} else if (type.compare("Toy2") == 0) {
		algorithm = new ToyAl2(id);
	} else if (type.compare("Toy3") == 0) {
		algorithm = new ToyAl3(id);
	} else if (type.compare("Toy4") == 0) {
		algorithm = new ToyAl4(id);
	} else {
		return NULL;
	}
	return algorithm;
}


	
};

