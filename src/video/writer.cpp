/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "writer.h"


using namespace SL;

Writer::Writer(InterfaceManager *iManager, cv::Mat &image)
{
	this->iManager = iManager;
	if(iManager->getOutputFile().size()!=0){
		output=true;
		outputFile=cv::Mat(image.rows, image.cols, CV_8UC3);
		if(image.rows%2==0)
			halfrows=image.rows/2;
		else
			halfrows=image.rows/2+1;
		writer=cv::VideoWriter(iManager->getOutputFile(),CV_FOURCC('P','I','M','1'),20.0, cv::Size(image.cols,image.rows));
	}else
		output=false;
}

