/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SL_SOFTWARERENDERER_H
#define SL_SOFTWARERENDERER_H


#include <vector>
#include <cv.h>

#include "../utils.hpp"
#include "source.h"
#include "../gui/glutwindow.h"

class InterfaceManager;
class MasksManager;
using namespace cv;
namespace SL
{

class SoftwareRenderer
{

public:
    SoftwareRenderer(InterfaceManager *iManager, MasksManager* mManager, cv::Mat &image);
    void extractBackgroundPhase( cv::Mat &image);
    void computeThresholdPhase( cv::Mat &image);
    void display (cv::Mat &image);
    void update_benchmark(int value);
    void prepare_frame();
    void update(int value);
private:
    cv::Mat background, foreground;
    cv::Mat speedlinesOverAll, currentSpeedlines;
    int splThickness;
    RGBPixel splColor;
    int splLength;
    bool computeBackground;
    int bgintervalcount;
    int bginterval;
    int bgFrameRequired;
    int widthScreen;
    int heightScreen;
    int whatShow;
    InterfaceManager *iManager;
    MasksManager* mManager;
    std::vector<cv::Mat>	bgframes;
    Source *source;
    GlutWindow *window;
    time_t start_time;
int					halfrows;
    bool				output;
    Mat					outputFile;
    BWPixel*			bin;
    VideoWriter			writer;
};

}

#endif // SL_SOFTWARERENDERER_H
