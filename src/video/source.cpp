/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "source.h"
#include <iostream>
using namespace SL;

Source::Source(	InterfaceManager*	iManager,MasksManager*		mManager)
{
	if ( iManager->camRequested() ){
		if ( iManager->getInputFile().size() == 0 ){
			
			// FRA
			#ifdef WIN32
			capture = VideoCapture(CV_CAP_ANY);
			#else
			capture = VideoCapture(CV_CAP_V4L2);
			#endif
			
		}
		else capture = VideoCapture(atoi(iManager->getInputFile().c_str()));
		if(iManager->getChangeResolution()){
			capture.set(CV_CAP_PROP_FRAME_WIDTH, iManager->getWidth());
			capture.set(CV_CAP_PROP_FRAME_HEIGHT, iManager->getHeight());
		}
	}
	else{
		capture = VideoCapture(iManager->getInputFile().c_str());
	}
	
	if (!capture.isOpened()){
		std::cerr << iManager->getInputFile() << ": Non existing or non supported input" << std::endl;
		exit(1);
	}
	
	std::clog << "Video source captured" << std::endl;
	//FRA
	std::clog << " Video Capture Dimension " << iManager->getWidth() << "x" << iManager->getHeight() <<std::endl;
}

