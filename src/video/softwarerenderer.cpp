/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "softwarerenderer.h"
#include <iostream>
#include <vector>
#include <highgui.h>

#include "../interfaceManager.hpp"
#include "../masksManager.hpp"
#include "../container.h"
#ifdef WIN32
#include <GL/GLee.h>
#else
#include <GL/gl.h>
#endif
#include <GL/freeglut.h>
#include <GL/glext.h>
using namespace cv;
namespace SL {

SoftwareRenderer::SoftwareRenderer(InterfaceManager *iManager, MasksManager* mManager, Mat &image) {
        this->iManager = iManager;
        this->mManager = mManager;

	foreground=mManager->getForeground();
	splThickness=iManager->getThicknessSpeedlines();
	splColor=iManager->getColorSpeedlines();
	splLength=iManager->getFadeStep();
	whatShow=SHOW_SCENE;
	computeBackground=false;
	bgintervalcount=0;
	bginterval=iManager->getBgExtractionInterval();
	bgFrameRequired=iManager->getBgExtractionFramesNext();
	widthScreen=image.cols;
	heightScreen=image.rows;
        source = new Source(iManager,mManager);
}

void SoftwareRenderer::extractBackgroundPhase(Mat &image){
	/*start background extraction*/
	std::clog << "Begin background extraction, please wait" << std::endl;
	std::vector<Mat> frames;
	for(int i=0; i< iManager->getBgExtractionFrames();i++){
                source->capture >> image;
		if(image.data==NULL)
			break;
		frames.push_back(image.clone());
	}
	background = extractBackgroundMean(frames);
#ifdef DEBUG
	imwrite("image/background.png", background);
#endif
	frames.clear();
	std::clog << "Background extraction finished" << std::endl;
}
void SoftwareRenderer::computeThresholdPhase( cv::Mat &image){
	std::clog << "Begin compute Threshold, please wait" << std::endl;
	std::vector<Mat> frames;
	for(int i=0; i< iManager->getThresholdFrame(); i++){
                source->capture >> image;
		if(image.data==NULL)
			break;
		frames.push_back(image.clone());
	}

	mManager->computeThreshold(frames, background);
	frames.clear();
	std::clog << "Threshold computed" << std::endl;

}
void SoftwareRenderer::update(int value){
	prepare_frame();
        //TODO
        //window->timer(0, update, 0);
}

void SoftwareRenderer::prepare_frame() {
    cv::Mat image;
        source->capture >> image;
	if(image.data!=NULL){
		mManager->addMask(image, background);
		currentSpeedlines = mManager->getCurrentSpeedlines();
		sumSpeedlinesWithFading(speedlinesOverAll, currentSpeedlines, splLength);
		
		if(computeBackground){
			bgintervalcount++;
			if(bgintervalcount>=bginterval){
				bgintervalcount=0;
				bgframes.push_back(image.clone());
				
				if(bgframes.size()>=bgFrameRequired){
					background.release();
					background=extractBackgroundMedian(bgframes);

                                        bgframes.clear();
					std::clog<<"background extracted"<<std::endl;
				}
			}
		}else if(bgframes.size()!=0){
			bgframes.clear();
			bgintervalcount=0;
		}
                glutPostRedisplay();
	}
}
void update_benchmark_it(int value) {}

void SoftwareRenderer::update_benchmark(int value) {
	static time_t old_time = start_time;
	static double fps;
	std::ostringstream v;
	time_t now = time(NULL);
	
	time_t seconds = now - start_time;
	if ( now > old_time ) {
		fps = value / seconds;
		old_time = now;
	}
	v << "Frame: " << value << " Time: " << seconds << " FPS: " << fps;
	//glutSetWindowTitle(v.str().c_str());
	std::string title = v.str();
	window->setWindowTitle( title );
	prepare_frame();
        window->timer(0, update_benchmark_it, ++value );
	
}

void SoftwareRenderer::display (cv::Mat &image)  {
	
	
	switch(whatShow){
		case SHOW_SCENE:
			glDrawPixels(image.cols, image.rows, GL_BGR, GL_UNSIGNED_BYTE, image.data);
			
			glBegin(GL_POINTS);
			for(register int y=0; y<speedlinesOverAll.rows;y++){
				for(register int x=0; x<speedlinesOverAll.cols;x++){
					bin=&MAT_ELEM(speedlinesOverAll, BWPixel, y, x);
					if(*bin!=BW_BLACK){
						glColor4ub(splColor.R, splColor.G, splColor.B, *bin);
						glVertex3i(x, speedlinesOverAll.rows-y, 1);
					}
				}
			}
			glEnd();
			glColor3ub(255, 255, 255);
			break;
		case SHOW_FOREGROUND:
			glDrawPixels(foreground.cols, foreground.rows, GL_LUMINANCE, GL_UNSIGNED_BYTE, foreground.data);
			break;
		case SHOW_BACKGROUND:
			glDrawPixels(background.cols, background.rows, GL_BGR, GL_UNSIGNED_BYTE, background.data);
			break;
		default:
			break;
	}
	if(output){
		glReadPixels(0, 0, outputFile.cols, outputFile.rows, GL_BGR, GL_UNSIGNED_BYTE, outputFile.data);
		/*upside frame*/
		RGBPixel tmp;
		for(register int y=0; y<=halfrows; y++){
			for(register int x=0; x<outputFile.cols; x++){
				tmp = MAT_ELEM(outputFile, RGBPixel, y, x);
				MAT_ELEM(outputFile, RGBPixel, y, x)= MAT_ELEM(outputFile, RGBPixel, outputFile.rows-y-1, x);
				MAT_ELEM(outputFile, RGBPixel, outputFile.rows-y-1, x)=tmp;
			}
		}
		writer << outputFile;
	}

}



};

