/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "container.h"
#include <iostream>
#include <cstdlib>
#include <highgui.h>
#include <GL/gl.h>

#define COLOR_CHANGE 10
#define LENGTH_CHANGE 10
#define THRESHOLD_CHANGE 5
#define DIM_CHANGE 1


namespace SL {

Container::Container(int argc, char* argv[])
{
    try {
        iManager=new InterfaceManager(argc,argv);
    } catch (BadParameterException e) {
        std::cerr<<e.what()<<std::endl;
        exit(1);
    }

    if ( iManager->helpRequested() ) {
        iManager->getUsage(stdout);
        exit(0);
    }
    speedlinesOverAll = cv::Mat(image.rows, image.cols, CV_8UC1, (Scalar)0);

}
void Container::printState(ostream& out)
{
    out << "Running with parameters: " << std::endl;
    out << *iManager << std::endl<<std::endl;
    out << "Key accepted: "<<std::endl;
}

void Container::keyboard(unsigned char key, int x, int y) {

    switch (key) {
    case 'q':
        if (splColor.R<=255-COLOR_CHANGE)
            splColor.R+=COLOR_CHANGE;
        break;
    case 'a':
        if (splColor.R>=COLOR_CHANGE)
            splColor.R-=COLOR_CHANGE;
        break;
    case 'w':
        if (splColor.G<=255-COLOR_CHANGE)
            splColor.G+=COLOR_CHANGE;
        break;
    case 's':
        if (splColor.G>=COLOR_CHANGE)
            splColor.G-=COLOR_CHANGE;
        break;
    case 'e':
        if (splColor.B<=255-COLOR_CHANGE)
            splColor.B+=COLOR_CHANGE;
        break;
    case 'd':
        if (splColor.B>=COLOR_CHANGE)
            splColor.B-=COLOR_CHANGE;
        break;
    case '0':
        splColor.R=0;
        splColor.G=0;
        splColor.B=0;
        break;
    case '1':
        splColor.B=255;
        splColor.G=0;
        splColor.R=0;
        break;
    case '2':
        splColor.B=0;
        splColor.G=255;
        splColor.R=0;
        break;
    case '3':
        splColor.B=255;
        splColor.G=255;
        splColor.R=0;
        break;
    case '4':
        splColor.B=0;
        splColor.G=0;
        splColor.R=255;
        break;
    case '5':
        splColor.B=255;
        splColor.G=0;
        splColor.R=255;
        break;
    case '6':
        splColor.B=0;
        splColor.G=255;
        splColor.R=255;
        break;
    case '7':
        splColor.B=255;
        splColor.G=255;
        splColor.R=255;
        break;
    case 'r':
        splThickness++;
        glPointSize(splThickness);
        break;
    case 'f':
        if (splThickness!=1) {
            splThickness--;
            glPointSize(splThickness);
        }
        break;
    case 't':
        if (splLength>=LENGTH_CHANGE)
            splLength-=LENGTH_CHANGE;
        break;
    case 'g':
        if (splLength<=255-LENGTH_CHANGE)
            splLength+=LENGTH_CHANGE;
        break;
    case 'u':
        if (mManager->getThreshold()<=255-THRESHOLD_CHANGE)
            mManager->setThreshold(mManager->getThreshold()+THRESHOLD_CHANGE);
        break;
    case 'j':
        if (mManager->getThreshold()>=THRESHOLD_CHANGE)
            mManager->setThreshold(mManager->getThreshold()-THRESHOLD_CHANGE);
        break;
    case 'y':
        mManager->setDimStructElement(mManager->getDimStructElement()+DIM_CHANGE);
        break;
    case 'h':
        mManager->setDimStructElement(mManager->getDimStructElement()-DIM_CHANGE);
        break;
    case 'i':
        bgFrameRequired++;
        break;
    case 'k':
        if (bgFrameRequired>=2)
            bgFrameRequired--;
        break;
    case 'o':
        bginterval++;
        break;
    case 'l':
        if (bginterval>=2)
            bginterval--;
        break;
    case 'n':
        // TODO
        //this-> computeThresholdPhase();
        break;
    case 'b':
        if (computeBackground)
            computeBackground=false;
        else
            computeBackground=true;
        break;
    case 'p':
        switch (whatShow) {
        case SHOW_SCENE:
            whatShow=SHOW_FOREGROUND;
            break;
        case SHOW_FOREGROUND:
            whatShow=SHOW_BACKGROUND;
            break;
        case SHOW_BACKGROUND:
            whatShow=SHOW_SCENE;
            break;
        default :
            break;
        }
        break;
    case 'x':
        std::clog<<"- disabling glut . . ."<<std::endl;
		pipeline->shutdown();

        //glutDestroyWindow();
        std::clog<<"- cleaning memory"<<std::endl;
        delete iManager;
        delete mManager;
        std::clog<<"- exiting "<<std::endl;
        exit(0);
        break;
    case 'c':
    default:
        break;
    }
    switch (key) {
    case 'q':
    case 'a':
    case 'w':
    case 's':
    case 'e':
    case 'd':
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
        std::clog<<"current Color= "<<(int)splColor.R<<":"<<(int)splColor.G<<":"<<(int)splColor.B<<std::endl;
        break;
    case 'r':
    case 'f':
        std::clog<<"current thickness= "<< splThickness <<std::endl;
        break;
    case 't':
    case 'g':
        std::clog<<"current length= "<< splLength <<std::endl;
        break;
    case 'y':
    case 'h':
        std::clog<<"current dimension structuring element= "<< mManager->getDimStructElement() <<std::endl;
        break;
    case 'u':
    case 'j':
        std::clog<<"current threshold= "<< mManager->getThreshold() <<std::endl;
        break;
    case 'b':
        if (computeBackground)
            std::clog<<"extract background ON"<<std::endl;
        else
            std::clog<<"extract background OFF"<<std::endl;
        break;

    case 'i':
    case 'k':
        std::clog<<"current frame required for background extraction= "<< bgFrameRequired <<std::endl;
        break;
    case 'o':
    case 'l':
        std::clog<<"current interval frame for background extraction= "<< bginterval <<std::endl;
        break;
    case 'p':
        switch (whatShow) {
        case SHOW_SCENE:
            std::clog<<"showing SCENE"<<std::endl;
            break;
        case SHOW_FOREGROUND:
            std::clog<<"showing FOREGROUND"<<std::endl;
            break;
        case SHOW_BACKGROUND:
            std::clog<<"showing BACKGROUND"<<std::endl;
            break;
        default :
            break;
        }
        break;
    case 'z':
        std::clog<<"b \t\t re-estimate BACKGROUND"<<std::endl;
        std::clog<<"n \t\t re-estimate THREASHOLD (requires no moving objects in the scene)"<<std::endl;
        std::clog<<"p \t\t show SCENE/FOREGROUND/BACKGROUND"<<std::endl;
        std::clog<<"q/a \t\t increase/decrease speedlines color RED"<<std::endl;
        std::clog<<"w/s \t\t increase/decrease speedlines color GREEN"<<std::endl;
        std::clog<<"e/d \t\t increase/decrease speedlines color BLUE"<<std::endl;
        std::clog<<"0 \t\t set speedlines color BLACK"<<std::endl;
        std::clog<<"1 \t\t set speedlines color BLUE"<<std::endl;
        std::clog<<"2 \t\t set speedlines color GREEN"<<std::endl;
        std::clog<<"3 \t\t set speedlines color AQUA"<<std::endl;
        std::clog<<"4 \t\t set speedlines color RED"<<std::endl;
        std::clog<<"5 \t\t set speedlines color FUCHSIA"<<std::endl;
        std::clog<<"6 \t\t set speedlines color YELLOW"<<std::endl;
        std::clog<<"7 \t\t set speedlines color WHITE"<<std::endl;
        std::clog<<"r/f \t\t increase/decrease speedlines THICKNESS"<<std::endl;
        std::clog<<"t/g \t\t increase/decrease speedlines LENGTH"<<std::endl;
        std::clog<<"y/h \t\t increase/decrease dimension STRUCTURING ELEMENT"<<std::endl;
        std::clog<<"u/j \t\t increase/decrease THRESHOLD"<<std::endl;
        std::clog<<"i/k \t\t increase/decrease BACKGROUND FRAME REQUIRED"<<std::endl;
        std::clog<<"o/l \t\t increase/decrease INTERVAL FRAME"<<std::endl;
        std::clog<<"x \t\t quit"<<std::endl;
        std::clog<<"z \t\t print this"<<std::endl;
    default:
        break;
    }
}
};
