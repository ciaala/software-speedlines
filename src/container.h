/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SL_CONTAINER_H
#define SL_CONTAINER_H
#include <highgui.h>
#include <cv.h>
#include "utils.hpp"
#include "interfaceManager.hpp"
#include "masksManager.hpp"
#include "pipeline/pipeline.h"
#define SHOW_SCENE 0
#define SHOW_FOREGROUND 1
#define SHOW_BACKGROUND 2
using namespace cv;

namespace SL
{

class Container
{
public:
        //Mat					background;
	Mat					image;
	Mat					speedlinesOverAll;
	Mat					currentSpeedlines;
	Mat					foreground;
//	Mat					outputFile;
//	BWPixel*			bin;
	RGBPixel			splColor;
	int					splThickness;
	int					splLength;
	int					whatShow;
	bool				computeBackground;
//	bool				output;

	int					bgintervalcount;
	int					bginterval;
	int					bgFrameRequired;
//	int					halfrows;
	int					widthScreen;
	int					heightScreen;

//VideoCapture		capture;
//	VideoWriter			writer;
	InterfaceManager*	iManager;
	MasksManager*		mManager;
public:
    Container(int argc, char* argv[]);
	void printState( std::ostream &out );
	void keyboard(unsigned char key, int x, int y);
private:
    void window();
    Pipeline *pipeline;
};

}

#endif // SL_CONTAINER_H
