/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "glutwindow.h"

#define COLOR_CHANGE 10
#define LENGTH_CHANGE 10
#define THRESHOLD_CHANGE 5
#define DIM_CHANGE 1
#define SHOW_SCENE 0
#define SHOW_FOREGROUND 1
#define SHOW_BACKGROUND 2

#ifdef WIN32
#include <GL/GLee.h>
#else
#include <GL/gl.h>
#endif
#include <GL/freeglut.h>
#include <GL/glext.h>
#include <iostream>
#include "../pipeline/renderer.h"

namespace SL {

GlutWindow::GlutWindow(int argc, char* argv[],InterfaceManager *iManager,Container *container)
{
    this->iManager = iManager;
    glutInit(&argc, argv);
    //setup();
    this->container = container;
}

void GlutWindow::init(cv::Mat &image) {



    glClearColor (0.0, 0.0, 0.0, 0.0);
    glClearDepth(1.0f);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, image.cols, 0.0, image.rows, -1.0, 2.0);
    glRasterPos2f(0, image.rows-1);
    glPixelZoom(1.0f, -1.0f);
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_BUFFER);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glPointSize(container->splThickness);

    glGenTextures(1, &bufferID);
    glBindTexture(GL_TEXTURE_2D, bufferID);
    glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, image.cols, image.rows, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}


void GlutWindow::reshape(int w, int h) {
    glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, w, h, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
    widthScreen=w;
    heightScreen=h;
}

void GlutWindow::keyboard(unsigned char key, int x, int y) {


}


void GlutWindow::display(cv::Mat &image, GLuint  ){

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, bufferID);
    glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 0, 0, image.cols, image.rows, 0);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, bufferID);
  //  glViewport(0, 0, widthScreen, heightScreen);
    glBegin(GL_QUADS);
		glMultiTexCoord2f(GL_TEXTURE0, 0, 0);
		glVertex2d(0, 0);
		glMultiTexCoord2f(GL_TEXTURE0, 0, 1);
		glVertex2d(0, image.rows);
		glMultiTexCoord2f(GL_TEXTURE0, 1, 1);
		glVertex2d(image.cols, image.rows);
		glMultiTexCoord2f(GL_TEXTURE0, 1, 0);
		glVertex2d(image.cols, 0);
    glEnd();
    glDisable(GL_TEXTURE_2D);
 //   glViewport(0, 0, image.cols, image.rows);

    glutSwapBuffers();
	glutPostRedisplay();
}
void reshape_it(int x,int y) {}
void display_it() {}
void keyboard_it(unsigned char key, int x, int y) {}
void update_benchmark(int value ) {}
void GlutWindow::setup(cv::Mat &image, Renderer *renderer) {

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(image.cols, image.rows);
    glutCreateWindow("Speedlines");
    init(image);
    glutReshapeFunc(reshape_it);
    glutDisplayFunc(display_it);
    glutKeyboardFunc(keyboard_it);
    //TODO
    //time(&start_time);
    glutTimerFunc(0, update_benchmark, 0);
    glutMainLoop();
}

void GlutWindow::timer(int time, void (*func)(int), int value) {


}
void GlutWindow::setWindowTitle(std::string &text) {}
};
