/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef SS_GLUTWINDOW_H
#define SS_GLUTWINDOW_H
#include <GL/freeglut.h>
#include <string>
#include "../interfaceManager.hpp"
#include "../masksManager.hpp"
#include "../pipeline/renderer.h"
#include "../container.h"
namespace SL {



class GlutWindow
{
private:
    Container *container;

	InterfaceManager* 	iManager;
	MasksManager*		mManager;
	std::string windowTitle;

	cv::Mat *currentImage;
    int widthScreen;
    int heightScreen;
	
	void setup(cv::Mat &image, Renderer *renderer);
	void init(cv::Mat &image);
	void display(cv::Mat &image, GLuint bufferID);
	GLuint bufferID;

public:
    GlutWindow(int argc, char* argv[], InterfaceManager* iManager,Container *container);
	void keyboard(unsigned char key,int x,int y);
	void reshape(int width, int height);
	void setWindowTitle(std::string &text);
	void timer( int time, void (*func)(int), int value );


};


}

#endif // SS_GLUTWINDOW_H
