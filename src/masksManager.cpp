#include "masksManager.hpp"


MasksManager::MasksManager(int k, int dimStruct, int rows, int cols):
k(k){
	this->colorThreshold=-1;
	this->percent=0.95;
	this->foreground = cv::Mat(rows, cols, CV_8UC1);
	this->open = cv::Mat(rows, cols, CV_8UC1);
	this->close= cv::Mat(rows, cols, CV_8UC1);
	this->structElement = cv::Mat(dimStruct, dimStruct, CV_8UC1, 1);
	this->edgeAlldx=cv::Mat(rows, cols, CV_16SC1);
	this->edgeAlldy=cv::Mat(rows, cols, CV_16SC1);
	this->edgeFirstdx=cv::Mat(rows, cols, CV_16SC1);
	this->edgeFirstdy=cv::Mat(rows, cols, CV_16SC1);
	this->edgeLastdx=cv::Mat(rows, cols, CV_16SC1);
	this->edgeLastdy=cv::Mat(rows, cols, CV_16SC1);
	this->currentSpeedlines=cv::Mat(rows, cols, CV_8UC1);
}


MasksManager::~MasksManager()
{
	this->clearMasks();
}

int MasksManager::getDimStructElement(){
	return this->structElement.rows;
}
void MasksManager::setDimStructElement(int dim){
	if(dim>1){
		this->structElement.release();
		this->structElement = cv::Mat(dim, dim, CV_8UC1, 1);
	}
}
cv::Mat MasksManager::getForeground(){
	return this->close;
}

int MasksManager::getThreshold(){
	return this->colorThreshold;
}
void MasksManager::setThreshold(int t){
	this->colorThreshold=t;
}

void MasksManager::computeThreshold(std::vector<cv::Mat> frames, cv::Mat background){
	/*si può migliorare*/
	int eachNum=50;
	int width=frames[0].cols;
	int height=frames[0].rows;
	int x, y;
	int stepX;
	srand(time(NULL));
	std::set<int> distanze;
	for(int i=0; i<frames.size();i++){
		x=0; y=0; stepX=0;
		while(y!=height){
			x = eachNum*stepX + rand() % eachNum;
			if(x>=width){
				y++;
				if(y==height)
					break;
				stepX=0;
			}else{
				distanze.insert(distanceColor(&MAT_ELEM(frames[i], RGBPixel, y, x), &MAT_ELEM(background, RGBPixel, y, x)));
				stepX++;
			}
		}
	}
	int k = distanze.size()*this->percent;
	std::set<int>::iterator iter=distanze.begin();
	for(int i=0; i<k-1; iter++, i++ );
	this->colorThreshold=*iter;
}
void MasksManager::addMask(cv::Mat image, cv::Mat background){
	if(this->colorThreshold==-1) throw ComputeThresholdFirstException("it's necessary to start computeThreshold before");

	int distance;
	for ( register int x = 0; x < image.cols; x++ ){
		for ( register int y = 0; y < image.rows; y++ ){
			distance = distanceColor(&MAT_ELEM(image, RGBPixel, y, x), &MAT_ELEM(background, RGBPixel, y, x));
			MAT_ELEM(foreground, BWPixel, y, x) = distance >= this->colorThreshold ? BW_WHITE : BW_BLACK;
		}
	}

	morphologyEx( this->foreground, this->open, 2, this->structElement);/*opening*/
	morphologyEx( this->open, this->close, 3, this->structElement);/*closure*/

#ifdef DEBUG
	imwrite("currentForeground.png",this->close);
#endif

	this->masks.push_back(close.clone());
	if ( this->masks.size() > this->k )
		this->masks.pop_front();
}


void MasksManager::clearMasks()
{
	this->masks.clear();
}


cv::Mat MasksManager::getCurrentSpeedlines() throw (NoMasksLoadedException)
{
	if ( this->masks.size() == 0 ) throw NoMasksLoadedException("No masks have been loaded");

	int width=this->masks[0].cols;
	int height=this->masks[0].rows;

	cv::Mat foregroundAggregate=this->masks[0].clone();

	for ( register int x = 0; x < width; x++ ){
		for ( register int y = 0; y < height; y++ ){
			for(int register i=1; i<this->masks.size(); i++)
				if ( MAT_ELEM(this->masks[i], BWPixel, y, x) == BW_WHITE ){
					MAT_ELEM(foregroundAggregate, BWPixel, y, x) = BW_WHITE;
					break;
				}
		}
	}

	/*cv::Canny(foregroundAggregate, this->edgeAll, 50, 60);
	cv::Canny(this->masks[0], this->edgeFirst, 50, 60);
	cv::Canny(this->masks[this->masks.size() - 1], this->edgeLast, 50, 60);*/

	cv::Sobel(foregroundAggregate, this->edgeAlldx, this->edgeAlldx.depth(), 1, 0, 1);
	cv::Sobel(foregroundAggregate, this->edgeAlldy, this->edgeAlldy.depth(), 0, 1, 1);
	cv::Sobel(this->masks[0], this->edgeFirstdx, this->edgeFirstdx.depth(), 1, 0, 1);
	cv::Sobel(this->masks[0], this->edgeFirstdy, this->edgeFirstdy.depth(), 0, 1, 1);
	cv::Sobel(this->masks[this->masks.size() - 1], this->edgeLastdx, this->edgeLastdx.depth(), 1, 0, 1);
	cv::Sobel(this->masks[this->masks.size() - 1], this->edgeLastdy, this->edgeLastdy.depth(), 0, 1, 1);
	//"masks", "first", "all", "last"

#ifdef DEBUG
	imshow("masks", foregroundAggregate);
	imwrite("SumForegrounds.png", foregroundAggregate);
	cv::Mat edgeAll=cv::Mat(height, width, CV_8UC1, (cv::Scalar)0);
	cv::Mat edgeFirst=cv::Mat(height, width, CV_8UC1, (cv::Scalar)0);
	cv::Mat edgeLast=cv::Mat(height, width, CV_8UC1, (cv::Scalar)0);

	for ( register int x = 0; x < width; x++ ){
		for ( register int y = 0; y < height; y++ ){
			if(MAT_ELEM(this->edgeAlldx, short, y, x) != BW_BLACK || MAT_ELEM(this->edgeAlldy, short, y, x) != BW_BLACK)
				MAT_ELEM(edgeAll, BWPixel, y, x) = BW_WHITE;
		}
	}
	imwrite("edgeAll.png", edgeAll);

	for ( register int x = 0; x < width; x++ ){
		for ( register int y = 0; y < height; y++ ){
			if(MAT_ELEM(this->edgeFirstdx, short, y, x) != BW_BLACK || MAT_ELEM(this->edgeFirstdy, short, y, x) != BW_BLACK)
				MAT_ELEM(edgeFirst, BWPixel, y, x) = BW_WHITE;
		}
	}
	imwrite("edgeFirst.png", edgeFirst);

	for ( register int x = 0; x < width; x++ ){
		for ( register int y = 0; y < height; y++ ){
			if(MAT_ELEM(this->edgeLastdx, short, y, x) != BW_BLACK || MAT_ELEM(this->edgeLastdy, short, y, x) != BW_BLACK)
				MAT_ELEM(edgeLast, BWPixel, y, x) = BW_WHITE;
		}
	}
	imwrite("edgeLast.png", edgeLast);
#endif

	for ( register int x = 0; x < width; x++ ){
		for ( register int y = 0; y < height; y++ ){
			if ( MAT_ELEM(this->edgeFirstdx, short, y, x) != BW_BLACK || MAT_ELEM(this->edgeLastdx, short, y, x) != BW_BLACK ||
					MAT_ELEM(this->edgeFirstdy, short, y, x) != BW_BLACK || MAT_ELEM(this->edgeLastdy, short, y, x) != BW_BLACK ||
					(MAT_ELEM(this->edgeAlldy, short, y, x) == BW_BLACK && MAT_ELEM(this->edgeAlldx, short, y, x) == BW_BLACK)){

				MAT_ELEM(this->currentSpeedlines, BWPixel, y, x) = BW_BLACK;

			}else
				MAT_ELEM(this->currentSpeedlines, BWPixel, y, x) = BW_WHITE;
		}
	}

#ifdef DEBUG
	imwrite("currentSpeedline.png", this->currentSpeedlines);
#endif

	return this->currentSpeedlines;
}

/*
IplImage* MasksManager::getMasksUnion()
{
	IplImage*	u;
	int			width;
	int			height;

	if ( this->masks.size() == 0 ) return NULL;

	width = this->masks[0]->width;
	height = this->masks[0]->height;
	u = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);

	for ( int x = 0; x < width; x++ )
	{
		for ( int y = 0; y < height; y++ )
		{
			*(Utils::getBWPixel(u, x, y)) = Utils::BW_BLACK;
			for ( int j = 0; j < this->masks.size(); j++ )
			{
				if ( *(Utils::getBWPixel(this->masks[j], x, y)) == Utils::BW_WHITE )
				{
					*(Utils::getBWPixel(u, x, y)) = Utils::BW_WHITE;
					break;
				}
			}
		}
	}

	return u;
}
*/
