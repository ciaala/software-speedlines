#include "interfaceManager.hpp"




InterfaceManager::InterfaceManager(int argc, char* argv[]) throw (BadParameterException)
{
	this->arg_help 						= arg_lit0(SHORT_HELP, LONG_HELP, "Display this usage information");
	this->arg_cam 						= arg_lit0(SHORT_CAM, LONG_CAM, "Uses webcam input instead of file input");
	this->input 						= arg_file0(SHORT_INPUT, LONG_INPUT, "video", "Uses file video");
	this->output 						= arg_file0(SHORT_OUTPUT, LONG_OUTPUT, "name file", "save output to file");
	this->detail						= arg_int0(SHORT_DETAIL, LONG_DETAIL, "", "Value for k parameter");
	this->bg_extraction_frame 			= arg_int0(SHORT_BG_EXTRACTION_FRAMES, LONG_BG_EXTRACTION_FRAMES, "", "Number of frames to be used for background extraction at start");
	this->bg_extraction_frame_next		= arg_int0(SHORT_BG_EXTRACTION_FRAMES_NEXT, LONG_BG_EXTRACTION_FRAMES_NEXT, "", "Number of frames to be used for background extraction during process");
	this->bg_extraction_interval		= arg_int0(SHORT_BG_EXTRACTION_INTERVAL, LONG_BG_EXTRACTION_INTERVAL, "", "Interval frames used for background extraction during process");
	this->threshold_extraction_frames	= arg_int0(SHORT_THRESHOLD_EXTRACTION_FRAMES, LONG_THRESHOLD_EXTRACTION_FRAMES, "","Number of frames to be used for threshold extraction");
	this->dim_struct_element			= arg_int0(SHORT_DIM_STRUCT_ELEMENT, LONG_DIM_STRUCT_ELEMENT, "", "Dimension of the structuring element, used for opening and closure");
	this->fade_step						= arg_int0(SHORT_FADE_STEP, LONG_FADE_STEP, "", "Value for speedlines fading step");
	this->color							= arg_str0(SHORT_COLOR, LONG_COLOR, "R:G:B", "Define the speedlines color");
	this->resolution					= arg_str0(SHORT_RESOLUTION_CAM, LONG_RESOLUTION_CAM, "WidthxHeight", "Define resolution cam (only if start with cam and be sure that your cam support it)");
	this->arg_thickness					= arg_int0(SHORT_THICKNESS, LONG_THICKNESS, "", "Define the thickness of the speedlines");
	this->end							= arg_end(20);


	this->argtable[0] = arg_help;
	this->argtable[1] = arg_cam;
	this->argtable[2] = input;
	this->argtable[3] = detail;
	this->argtable[4] = bg_extraction_frame;
	this->argtable[5] = bg_extraction_frame_next;
	this->argtable[6] = bg_extraction_interval;
	this->argtable[7] = threshold_extraction_frames;
	this->argtable[8] = dim_struct_element;
	this->argtable[9] = fade_step;
	this->argtable[10] = color;
	this->argtable[11] = arg_thickness;
	this->argtable[12] = output;
	this->argtable[13] = resolution;
	this->argtable[14] = end;

	if (arg_nullcheck(argtable) != 0)
		exit(0);

	this->input->filename[0]="";
	this->output->filename[0]="";
	this->detail->ival[0]= InterfaceManager::DEFAULT_K_VALUE;
	this->bg_extraction_frame->ival[0]=InterfaceManager::DEFAULT_BG_EXTRACTION_FRAMES_VALUE;
	this->bg_extraction_frame_next->ival[0]=InterfaceManager::DEFAULT_BG_EXTRACTION_FRAMES_NEXT;
	this->bg_extraction_interval->ival[0]=InterfaceManager::DEFAULT_BG_EXTRACTION_FRAMES_INTERVAL;
	this->threshold_extraction_frames->ival[0]=InterfaceManager::DEFAULT_THRESHOLDFRAME;
	this->dim_struct_element->ival[0]= InterfaceManager::DEFAULT_DIM_STRUCT_ELEMENT;
	this->fade_step->ival[0]= InterfaceManager::DEFAULT_FADE_STEP;
	this->color->sval[0]= "255:255:255";
	this->arg_thickness->ival[0]= InterfaceManager::DEFAULT_THICKNESS;
	this->resolution->sval[0]= "";

	int nerrors = arg_parse(argc,argv,argtable);
	if(nerrors>0){
        arg_print_errors(stdout, end, argv[0]);
        throw BadParameterException("Bad parameter");
    }

	this->program_name=argv[0];
	if(this->arg_help->count > 0)
		this->help= true;
	else
		this->help= false;
	this->inputFile=this->input->filename[0];
	if(this->inputFile.size()==0)
		this->cam= true;
	else{
		if(this->arg_cam->count > 0)
			this->cam= true;
		else
			this->cam=false;
	}
	this->outputFile=this->output->filename[0];
	this->k=this->detail->ival[0];
	if ( this->k < InterfaceManager::MIN_K_VALUE )
		throw BadParameterException(std::string("Bad ") + LONG_DETAIL + " parameter");
	this->bgExtractionFrames=this->bg_extraction_frame->ival[0];
	if ( this->bgExtractionFrames < InterfaceManager::MIN_BG_EXTRACTION_FRAMES_VALUE )
		throw BadParameterException(std::string("Bad ") + LONG_BG_EXTRACTION_FRAMES + " parameter");
	this->bgExtractionFramesNext=this->bg_extraction_frame_next->ival[0];
	if ( this->bgExtractionFramesNext < InterfaceManager::MIN_BG_EXTRACTION_FRAMES_NEXT )
		throw BadParameterException(std::string("Bad ") + LONG_BG_EXTRACTION_FRAMES_NEXT + " parameter");
	this->bgExtractionInterval=this->bg_extraction_interval->ival[0];
	if ( this->bgExtractionInterval < InterfaceManager::MIN_BG_EXTRACTION_FRAMES_INTERVAL )
		throw BadParameterException(std::string("Bad ") + LONG_BG_EXTRACTION_INTERVAL + " parameter");
	this->thresholdFrame=this->threshold_extraction_frames->ival[0];
	if ( this->thresholdFrame < InterfaceManager::MIN_THRESHOLDFRAME)
		throw BadParameterException(std::string("Bad ") + LONG_THRESHOLD_EXTRACTION_FRAMES + " parameter");
	this->dimStructElement=this->dim_struct_element->ival[0];
	if ( this->dimStructElement < InterfaceManager::MIN_DIM_STRUCT_ELEMENT)
		throw BadParameterException(std::string("Bad ") + LONG_DIM_STRUCT_ELEMENT + " parameter");
	this->fadeStep=this->fade_step->ival[0];
	if ( this->fadeStep < InterfaceManager::MIN_FADE_STEP || this->fadeStep > InterfaceManager::MAX_FADE_STEP)
		throw BadParameterException(std::string("Bad ") + LONG_FADE_STEP + " parameter");
	this->thickness=this->arg_thickness->ival[0];
	if ( this->thickness < InterfaceManager::MIN_THICKNESS )
		throw BadParameterException(std::string("Bad ") + LONG_THICKNESS + " parameter");

	std::string colorString=this->color->sval[0];
	int cl = atoi(colorString.substr(0, colorString.find(":")).c_str());
	colorString = colorString.substr(colorString.find(":") + 1);
	if ( cl < InterfaceManager::MIN_COLOR || cl> InterfaceManager::MAX_COLOR)
		throw BadParameterException(std::string("Bad ") + LONG_COLOR + " parameter");
	this->colorSpeedlines.R=cl;

	cl = atoi(colorString.substr(0, colorString.find(":")).c_str());
	colorString = colorString.substr(colorString.find(":") + 1);
	if ( cl < InterfaceManager::MIN_COLOR || cl> InterfaceManager::MAX_COLOR)
		throw BadParameterException(std::string("Bad ") + LONG_COLOR + " parameter");
	this->colorSpeedlines.G=cl;

	cl = atoi(colorString.c_str());
	if ( cl < InterfaceManager::MIN_COLOR || cl> InterfaceManager::MAX_COLOR)
		throw BadParameterException(std::string("Bad ") + LONG_COLOR + " parameter");
	this->colorSpeedlines.B=cl;

	std::string resolutionCam=this->resolution->sval[0];
	if(resolutionCam.size()>0){
		cl=atoi(resolutionCam.substr(0, resolutionCam.find("x")).c_str());
		resolutionCam = resolutionCam.substr(resolutionCam.find(":") + 1);
		if(cl<=0)
			throw BadParameterException(std::string("Bad ") + LONG_RESOLUTION_CAM + " parameter");
		this->width=cl;
		cl=atoi(resolutionCam.c_str());
		if(cl<=0)
			throw BadParameterException(std::string("Bad ") + LONG_RESOLUTION_CAM + " parameter");
		this->height=cl;
		this->changeResolution=true;
	}else
		this->changeResolution=false;
}


InterfaceManager::~InterfaceManager()
{
	arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));
}
int InterfaceManager::getWidth(){
	return this->width;
}
int InterfaceManager::getHeight(){
	return this->height;
}
bool InterfaceManager::getChangeResolution(){
	return this->changeResolution;
}

std::string InterfaceManager::getInputFile()
{
	return this->inputFile;
}
std::string InterfaceManager::getOutputFile(){
	return this->outputFile;
}

bool InterfaceManager::helpRequested()
{
	return this->help;
}


bool InterfaceManager::camRequested()
{
	return this->cam;
}



int InterfaceManager::getK()
{
	return this->k;
}


int InterfaceManager::getBgExtractionFrames()
{
	return this->bgExtractionFrames;
}
int InterfaceManager::getBgExtractionFramesNext(){
	return this->bgExtractionFramesNext;
}
int InterfaceManager::getBgExtractionInterval(){
	return this->bgExtractionInterval;
}

int InterfaceManager::getDimStructElement(){return this->dimStructElement;}

int InterfaceManager::getFadeStep()
{
	return this->fadeStep;
}
RGBPixel InterfaceManager::getColorSpeedlines(){return this->colorSpeedlines;}
int InterfaceManager::getThicknessSpeedlines(){return this->thickness;}
int InterfaceManager::getThresholdFrame(){return this->thresholdFrame;}

void InterfaceManager::getUsage(FILE* outputstream)
{

	fprintf(outputstream, "Usage: \n");
	fprintf(outputstream, "%s [options] -%s filename\n", this->program_name.c_str(), SHORT_INPUT);
	fprintf(outputstream, "%s [options] -%s [-%s cam]\n", this->program_name.c_str(), SHORT_CAM, SHORT_INPUT);

	arg_print_glossary(outputstream, argtable, "  %-25s %s\n");

}


std::ostream& operator<<(std::ostream& os, const InterfaceManager& im)
{
	if ( im.cam == true )
	{
		if ( im.inputFile.size() == 0 ) os << "cam input:\t\t\t\t\t\t" << "Default cam" << std::endl;
		else os << "cam input:\t\t\t\t\t\t" << im.inputFile << std::endl;
	}
	else
	{
		os << "input file:\t\t\t\t\t\t" << im.inputFile << std::endl;
	}
	if(im.outputFile.size()!=0)
		os << "output file:\t\t\t\t\t\t" << im.outputFile << std::endl;

	os << "k value:\t\t\t\t\t\t"						<< im.k						<< std::endl;
	os << "frames for background extraction at start:\t\t"<< im.bgExtractionFrames	<< std::endl;
	os << "frames for background extraction during process:\t"<< im.bgExtractionFramesNext	<< std::endl;
	os << "interval frames for background extraction:\t\t"<< im.bgExtractionInterval	<< std::endl;
	os << "frames for threshold extraction:\t\t\t"		<< im.thresholdFrame	<< std::endl;
	os << "dimension of structuring element\t\t\t"		<< im.dimStructElement	<< std::endl;
	os << "fading step:\t\t\t\t\t\t"					<< im.fadeStep			<< std::endl;
	os << "color speedlines:\t\t\t\t\t"						<< (int)im.colorSpeedlines.R<<":"<< (int)im.colorSpeedlines.G<<":"<<(int)im.colorSpeedlines.B<< std::endl;
	os << "thickness speedlines\t\t\t\t\t"					<< im.thickness	;

	return os;
}
