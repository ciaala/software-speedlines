#ifndef INTERFACE_MANAGER_HPP
#define INTERFACE_MANAGER_HPP

#include <cstdlib>
#include <sstream>
#include <cstring>
#include <climits>
#include <argtable2.h>
#include "exceptions.hpp"
#include "utils.hpp"


#define LONG_HELP							"help"
#define LONG_CAM							"cam"
#define LONG_RESOLUTION_CAM					"resolution"
#define LONG_INPUT							"input"
#define LONG_OUTPUT							"output"
#define LONG_DETAIL							"detail"
#define LONG_BG_EXTRACTION_FRAMES			"bgframes"
#define LONG_BG_EXTRACTION_FRAMES_NEXT		"bgnext"
#define LONG_BG_EXTRACTION_INTERVAL			"bginterval"
#define LONG_THRESHOLD_EXTRACTION_FRAMES 	"threshold"
#define LONG_DIM_STRUCT_ELEMENT				"dimStructElement"
#define LONG_FADE_STEP						"fade"
#define LONG_COLOR							"color"
#define LONG_THICKNESS						"thickness"

#define SHORT_HELP							"h"
#define SHORT_CAM							"c"
#define SHORT_RESOLUTION_CAM				"r"
#define SHORT_INPUT							"i"
#define SHORT_OUTPUT						"o"
#define SHORT_DETAIL						"k"
#define SHORT_BG_EXTRACTION_FRAMES			"b"
#define SHORT_BG_EXTRACTION_FRAMES_NEXT		"n"
#define SHORT_BG_EXTRACTION_INTERVAL		"e"
#define SHORT_DIM_STRUCT_ELEMENT			"d"
#define SHORT_FADE_STEP						"f"
#define SHORT_COLOR							"l"
#define SHORT_THICKNESS						"s"
#define SHORT_THRESHOLD_EXTRACTION_FRAMES 	"t"

#define NUM_OPTIONS							15


class InterfaceManager
{
	protected:
	std::string		program_name;
	std::string		inputFile;
	bool			help;
	bool			cam;
	bool			changeResolution;
	std::string		outputFile;
	int				k;
	int				bgExtractionFrames;
	int				bgExtractionFramesNext;
	int				bgExtractionInterval;
	int				dimStructElement;
	int				fadeStep;
	RGBPixel		colorSpeedlines;
	int				thickness;
	int				thresholdFrame;
	int				width;
	int				height;

	static const bool			DEFAULT_HELP_VALUE						= false;
	static const bool			DEFAULT_CAM_VALUE						= false;
	static const int			DEFAULT_K_VALUE							= 3;
	static const int			DEFAULT_BG_EXTRACTION_FRAMES_VALUE		= 25;
	static const int			DEFAULT_BG_EXTRACTION_FRAMES_NEXT		= 10;
	static const int			DEFAULT_BG_EXTRACTION_FRAMES_INTERVAL	= 10;
	static const int			DEFAULT_DIM_STRUCT_ELEMENT				= 3;
	static const int			DEFAULT_FADE_STEP						= 10;
	static const int			DEFAULT_THICKNESS						= 1;
	static const int			DEFAULT_THRESHOLDFRAME					= 15;

	static const int			MIN_K_VALUE								= 3;
	static const int			MIN_BG_EXTRACTION_FRAMES_VALUE			= 1;
	static const int			MIN_BG_EXTRACTION_FRAMES_NEXT			= 1;
	static const int			MIN_BG_EXTRACTION_FRAMES_INTERVAL		= 0;
	static const int			MIN_DIM_STRUCT_ELEMENT					= 1;
	static const int			MIN_FADE_STEP							= 1;
	static const int			MIN_COLOR								= 0;
	static const int			MIN_THICKNESS							= 1;
	static const int			MIN_THRESHOLDFRAME						= 1;

	static const int			MAX_K_VALUE								= INT_MAX;
	static const int			MAX_BG_EXTRACTION_FRAMES_VALUE			= INT_MAX;
	static const int			MAX_BG_EXTRACTION_FRAMES_NEXT			= INT_MAX;
	static const int			MAX_BG_EXTRACTION_FRAMES_INTERVAL		= INT_MAX;
	static const int			MAX_DIM_STRUCT_ELEMENT					= INT_MAX;
	static const int			MAX_FADE_STEP							= 255;
	static const int			MAX_COLOR								= 255;
	static const int			MAX_THICKNESS							= INT_MAX;
	static const int			MAX_THRESHOLDFRAME						= INT_MAX;

	private:
	struct arg_lit 	*arg_help;
	struct arg_lit 	*arg_cam;
	struct arg_file *input;
	struct arg_file *output;
	struct arg_int 	*detail;
	struct arg_int	*bg_extraction_frame;
	struct arg_int  *bg_extraction_frame_next;
	struct arg_int  *bg_extraction_interval;
	struct arg_int	*threshold_extraction_frames;
	struct arg_int	*dim_struct_element;
	struct arg_int	*fade_step;
	struct arg_str	*color;
	struct arg_str	*resolution;
	struct arg_int	*arg_thickness;
	struct arg_end	*end;
	void* argtable[NUM_OPTIONS];


	public:
	InterfaceManager(int argc, char* argv[]) throw (BadParameterException);
	~InterfaceManager();

	std::string getInputFile();
	std::string getOutputFile();
	bool helpRequested();
	bool camRequested();
	bool getChangeResolution();
	int getWidth();
	int getHeight();
	int getK();
	int getBgExtractionFrames();
	int getBgExtractionFramesNext();
	int getBgExtractionInterval();
	int getDimStructElement();
	int getFadeStep();
	RGBPixel getColorSpeedlines();
	int getThicknessSpeedlines();
	int getThresholdFrame();
	void getUsage(FILE* outputstream);

	friend std::ostream& operator<<(std::ostream& os, const InterfaceManager& im);
};

#endif // INTERFACE_MANAGER_HPP
