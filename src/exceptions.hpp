
#ifndef EXCEPTIONS_HPP_
#define EXCEPTIONS_HPP_

#include <stdexcept>
#include <string>


class BadParameterException : public std::runtime_error
{
	public:
	BadParameterException(const std::string& s):
	runtime_error(s)
	{}
};


class NegativeBGExtractionFramesException : public std::runtime_error
{
	public:
	NegativeBGExtractionFramesException(const std::string& s):
	runtime_error(s)
	{}
};


class NeedMoreFramesException : public std::runtime_error
{
	public:
	NeedMoreFramesException(const std::string& s):
	runtime_error(s)
	{}
};

class ComputeThresholdFirstException : public std::runtime_error
{
	public:
	ComputeThresholdFirstException(const std::string& s):
	runtime_error(s)
	{}
};

class NoMasksLoadedException : public std::runtime_error
{
	public:
	NoMasksLoadedException(const std::string& s):
	runtime_error(s)
	{}
};


class TooManySnaphotsException : public std::runtime_error
{
	public:
	TooManySnaphotsException(const std::string& s):
	runtime_error(s)
	{}
};


#endif /* EXCEPTIONS_HPP_ */
