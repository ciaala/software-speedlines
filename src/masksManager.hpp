
#ifndef MASKSMANAGER_HPP_
#define MASKSMANAGER_HPP_

#include <cstdlib>
#include <deque>
#include <vector>
#include <cv.h>
#include <highgui.h>
#include <time.h>
#include "exceptions.hpp"
#include "utils.hpp"


class MasksManager
{
	private:
	int						k;
	int						colorThreshold;
	double percent;
	std::deque<cv::Mat>		masks;
	cv::Mat					foreground;
	cv::Mat					open;
	cv::Mat					close;
	cv::Mat					structElement;
	cv::Mat					edgeAlldx;
	cv::Mat					edgeAlldy;
	cv::Mat					edgeFirstdx;
	cv::Mat					edgeFirstdy;
	cv::Mat					edgeLastdx;
	cv::Mat					edgeLastdy;
	cv::Mat 				currentSpeedlines;

	//IplImage* getMasksUnion();


	public:
	MasksManager(int k, int dimStruct, int rows, int cols);
	~MasksManager();

	void computeThreshold(std::vector<cv::Mat> frames, cv::Mat background);
	void addMask(cv::Mat image, cv::Mat background);/*add foreground*/
	void clearMasks();
	cv::Mat getCurrentSpeedlines() throw (NoMasksLoadedException);
	int getDimStructElement();
	void setDimStructElement(int dim);
	cv::Mat getForeground();
	int getThreshold();
	void setThreshold(int t);

};

#endif /* MASKSMANAGER_HPP_ */
