
// #include <sstream>
// #include <cstdlib>
// #include <cstring>
// #include "time.h"


// #include <vector>

// #include "exceptions.hpp"
#include "interfaceManager.hpp"
#include "masksManager.hpp"
// #include "utils.hpp"




#include <iostream>
#include "gui/glutwindow.h"
#include "container.h"
#include "video/softwarerenderer.h"


// unsigned int bufferID;

/** FRANCESCO **/
time_t start_time;


/** **/

// void extractBackgroundPhase();
// void computeThresholdPhase();
// void display();
// void init();
// void keyboard(unsigned char key, int x, int y);
// void update(int value);
// void update_benchmark(int value);
// void prepare_frame();
// void reshape(int w, int h);




int main(int argc, char* argv[])
{
	SL::Container container(argc,argv);
        SL::GlutWindow window(argc, argv, container.iManager, &container );
	SL::SoftwareRenderer softwareRenderer(container.iManager,container.mManager,container.image);

	
	
	window.keyboard('z',0,0);

        softwareRenderer.extractBackgroundPhase(container.image);
	
	
	InterfaceManager* iManager = container.iManager;
	MasksManager mManager(iManager->getK(), iManager->getDimStructElement(), container.image.rows, container.image.cols);

        softwareRenderer.computeThresholdPhase(container.image);

	

	//TODO
#ifdef DEBUG_NO
	cvNamedWindow("masks", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("frame", CV_WINDOW_AUTOSIZE);

	while(1){
		capture >> image;
		imshow("frame", image);
		imwrite("frame.png", image);
		mManager->addMask(image, background);
		currentSpeedlines = mManager->getCurrentSpeedlines();
		sumSpeedlinesWithFading(speedlinesOverAll, currentSpeedlines, iManager->getFadeStep());
		imwrite("renderingSpeedlines.png", speedlinesOverAll);
		if(waitKey(30)>0) break;
	}
	keyboard('x',0,0);
#endif



	return 0;
}




