
include_directories(${speedlines_SOURCE_DIR}/pipeline)
add_executable(test_parser test_parser.cpp)
add_executable(pair_d_parser pair_d_parser.cpp)

target_link_libraries(test_parser pipeline)

