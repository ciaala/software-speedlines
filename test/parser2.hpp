#define BOOST_SPIRIT_DEBUG
#include <map>
#include <iostream>

#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/foreach.hpp>
#include <boost/spirit/include/phoenix_bind.hpp>

namespace spirit = boost::spirit;
namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;
namespace phoenix = boost::phoenix;



using ascii::char_;
using qi::lit;
using qi::_1;
using qi::double_;

using qi::on_error;
using qi::accept;
using phoenix::val;
using phoenix::construct;
using phoenix::bind;
using qi::lexeme;
using qi::eol;
using ascii::blank;
using ascii::space;

// typedef std::map<std::string, std::string> map_s_type;
// typedef std::pair<std::string, std::string> pair_s_type;
// typedef std::map<std::string, double> map_d_type;
// typedef std::pair<std::string, double> pair_d_type;
typedef std::list<std::string>::iterator s_list_it;

typedef boost::variant< std::string, double > value_type;
typedef std::map<std::string, value_type> map_v_type;
typedef std::pair<std::string, value_type> pair_v_type;

// struct pstage{
// 	std::string type;
// 	std::string name;
// 	map_s_type params;
// 	//map_d_type paramd;
// };
// 
// //paird_type paramd;
// BOOST_FUSION_ADAPT_STRUCT(
// 	pstage,
// 	(std::string, type)
// 	(std::string, name)
// 	(map_s_type, params)
// 	//(map_d_type, paramd)
// )
//	
struct pstage{
	std::string type;
	std::string name;
	map_v_type paramv;
	//map_d_type paramd;
};

//paird_type paramd;
BOOST_FUSION_ADAPT_STRUCT(
	pstage,
	(std::string, type)
	(std::string, name)
	(map_v_type, paramv)
	//(map_d_type, paramd)
)
class sbuilder : public boost::static_visitor<> {
public:

	const std::string *current_key;
	
	void operator()(std::string value) {
		std::cout << *current_key << " = " << value <<std::endl;
	};
	
	void operator()(double value ){
		std::cout << *current_key << " = " << value << std::endl;
	};


	
    void __stage( pstage &_stage ) {
		std::string &type = _stage.type;
		std::string &name = _stage.name;
		map_v_type &m = _stage.paramv;

        std::cout << "-------------------------------- \n";
        std::cout << "Parsing Stage, entries:" << m.size() << std::endl;
		map_v_type::iterator end = m.end();
        for (map_v_type::iterator it = m.begin(); it != end; ++it)
        {
			current_key = &((*it).first);
			value_type second = (*it).second;
            if (!second.empty()) {
				second.apply_visitor<>(*this);
			} else {
				std::cout << *current_key << std::endl;
			}
            
        }
        std::cout << "---------------------------------\n";
    }

    void __connect(std::list<std::string> &chain) {
        std::cout << "-------------------------------- \n";
        std::cout << "Parsing [Connect]\n";
        {
            s_list_it it;
            s_list_it end = chain.end();
            for ( it = chain.begin( ); it != end; ++it ) {
                std::cout << (*it) << " >> ";
            }
            std::cout << std::endl;
        }
        std::cout << "---------------------------------\n";
    }

    void __comment(std::string &text ) {
        std::cout << "-------------------------------- \n";
        std::cout << "Parsing [Comment]\n";
        {
            std::cout << text << std::endl;
        }
        std::cout << "---------------------------------\n";
    }
};




//(map<std::string,std::string> , params)

template <typename Iterator>
struct keys_and_values : qi::grammar<Iterator>
{

//     qi::rule<Iterator, map_s_type()> params;
// 	qi::rule<Iterator, map_d_type()> paramd;
//     qi::rule<Iterator, pair_s_type()> pairs;
// 	qi::rule<Iterator, pair_d_type()> paird;
// 	

	qi::rule<Iterator, map_v_type()> paramv;
	qi::rule<Iterator, pair_v_type()> pairv;
	
    //qi::rule<Iterator, std::string()> key, value;
	qi::rule<Iterator, value_type()> value;
	
	qi::rule<Iterator, std::string()> qstring;
	qi::rule<Iterator, std::string()> sstring;
	qi::rule<Iterator> skip;
    qi::rule<Iterator> start;
    qi::rule<Iterator, std::list<std::string>() > connect;
	qi::rule<Iterator, pstage()> stage;
    qi::rule<Iterator> comment;
	qi::rule<Iterator,std::string()> comment1;
	qi::rule<Iterator,std::string()> comment2;
	qi::rule<Iterator,std::string()> comment3;


	
    keys_and_values(sbuilder *bb) : keys_and_values::base_type(start)
    {

		skip = *(ascii::blank | qi::eol);
    
		sstring %= skip >> char_("a-zA-Z_") >> *char_("a-zA-Z_0-9");// >> *(' ' | qi::eol);
		qstring %= skip >> lexeme[lit('"') >> *(char_ - '"') >> lit('"')];// >> *(' ' | qi::eol);
		

// 		comment %= lit("/*") >> *(char_ - "*/") >> lit("*/")[ phoenix::bind(&sbuilder::__comment, bb, qi::_val) ]
// 				|| lit('#') >> *(char_ - '\n') >> lit('\n')[ phoenix::bind(&sbuilder::__comment, bb, qi::_val) ]
// 				|| lit("//") >> *(char_ - '\n') >> lit('\n')[ phoenix::bind(&sbuilder::__comment, bb, qi::_val) ];

		comment1 %= lit("/*") >> *(char_ - "*/") >> lit("*/")[ phoenix::bind(&sbuilder::__comment, bb, qi::_val) ];
		comment2 %= lit('#') >> *(char_ - '\n')> lit('\n')[ phoenix::bind(&sbuilder::__comment, bb, qi::_val) ];
		comment3 %= lit("//") >> *(char_ - '\n') > lit('\n')[ phoenix::bind(&sbuilder::__comment, bb, qi::_val) ];
// 		comment1 %= lit("/*") >> *(char_ - "*/") >> lit("*/");
//  	comment2 %= lit('#') >> *(char_ - '\n') >> lit('\n');
// 		comment3 %= lit("//") >> *(char_ - '\n') >> lit('\n');
		comment = comment1 | comment2 | comment3;

		//connect = lit("connect");

		
       // connect %= sstring >> +(lit(">>") >> sstring) >> lit(';');
		//chainer %= *(ascii::space) >> lit(">>") >> *(ascii::space);

		connect %= sstring % ( skip >> lit(">>")) >> skip >>lit(';')[phoenix::bind(&sbuilder::__connect,bb, qi::_val)];
		


// 		paird %= sstring >> (skip >> double_ );
// 	
// 		pairs %= sstring >> -(skip >> lit('=') >> qstring);
// 				
// 		params %= pairs % (skip >> lit(',')) ;
// 
// 		paramd %= paird %( skip >> lit(','));
// 		
// 		paramd.name("paramd");
//
		value = (qstring | skip >> double_  );
		pairv %= (sstring|qstring) >> skip >> -( lit('=') >> value ) ;
		paramv %= pairv % lit(',');
		stage %=  sstring >> sstring >> -(skip >> lit('{') >> paramv >> skip >> -(lit(',') >> skip) >> lit('}') )
			>> skip	>> lit(';')[phoenix::bind(&sbuilder::__stage, bb,qi::_val)];
		
		//[phoenix::bind(&sbuilder::__stage, bb,qi::_1 )];
		start = +( +blank | +qi::eol |lit("stage ") >> stage | lit("connect ") >> connect | comment);

		BOOST_SPIRIT_DEBUG_NODE(start);
		BOOST_SPIRIT_DEBUG_NODE(stage);
		BOOST_SPIRIT_DEBUG_NODE(comment);
		BOOST_SPIRIT_DEBUG_NODE(connect);
// 		BOOST_SPIRIT_DEBUG_NODE(params);
// 		BOOST_SPIRIT_DEBUG_NODE(paramd);
		BOOST_SPIRIT_DEBUG_NODE(sstring);
		BOOST_SPIRIT_DEBUG_NODE(qstring);
// 		BOOST_SPIRIT_DEBUG_NODE(pairs);
	//	BOOST_SPIRIT_DEBUG_NODE(paird);
		
		BOOST_SPIRIT_DEBUG_NODE(skip);

		BOOST_SPIRIT_DEBUG_NODE(pairv);
		BOOST_SPIRIT_DEBUG_NODE(paramv);
		BOOST_SPIRIT_DEBUG_NODE(value);
	}

};


