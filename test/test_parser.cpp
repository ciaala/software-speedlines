//#include "../pipeline/pipelineloader.h"
//#include "../pipeline/pipelinescheduler.h"
#include <fstream>
/*
#include <boost/spirit/include/classic_iterator.hpp>*/
#include "parser2.hpp"


namespace spirit = boost::spirit;




int main(int argc, char* argv[]) {
// 	ff::PipelineLoader pl(NULL);
// 	std::string filename(argv[1]);
// 	ff::PipelineScheduler* scheduler =  pl.interpret(filename);
	//scheduler->start();
// 	std::string input("key1=value1;key2;key3=value3");  // input to parse
// 	std::string::iterator begin = input.begin();
// 	std::string::iterator end = input.end();
	if ( argc != 2 )
		return 0;
	std::ifstream file(argv[1], std::ios_base::in);
	file.unsetf(std::ios::skipws);
	spirit::istream_iterator begin(file);
	spirit::istream_iterator end;
	sbuilder bb;
	keys_and_values<spirit::istream_iterator > p(&bb) ;    // create instance of parser
	std::map<std::string, std::string> m;        // map to receive results
	bool result = qi::parse(begin, end, p, m);   // returns true if successful


	if ( !result )
	{
		std::cout << "-------------------------------- \n";
		std::cout << "Parsing failed\n";
		std::cout << "-------------------------------- \n";
	}
	else
	{
		std::cout << "-------------------------------- \n";
		std::cout << "Parsing completed\n";
		std::cout << "-------------------------------- \n";
	}
	return 0;
}
