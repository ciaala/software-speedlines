#define BOOST_SPIRIT_DEBUG
#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/spirit/include/qi_real.hpp>
#include <iostream>
#include <fstream>
#include <map>


typedef std::map<std::string, double> map_d_type;
typedef std::pair<std::string, double> pair_d_type;

namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;
namespace spirit = boost::spirit;


using ascii::char_;

template <typename Iterator>
struct keys_and_values : qi::grammar<Iterator,map_d_type()>
{
	qi::rule<Iterator, map_d_type()> paramd;
    qi::rule<Iterator, pair_d_type()> paird;
    qi::rule<Iterator, std::string()> sstring;
    qi::rule<Iterator> skip;
	
    keys_and_values() :  keys_and_values::base_type(paramd)
    {


		sstring %= qi::skip[char_("a-zA-Z_") >> *char_("a-zA-Z_0-9")];
		paird %= qi::skip(ascii::space)[ sstring >> qi::lit('=') >> qi::double_ ] ;
		paramd %= qi::skip[ paird % qi::lit(',') >> qi::lit(';')] ;
		
		//BOOST_SPIRIT_DEBUG_NODE(skip);
		BOOST_SPIRIT_DEBUG_NODE(paird);
		BOOST_SPIRIT_DEBUG_NODE(paramd);
		BOOST_SPIRIT_DEBUG_NODE(sstring);
    }

};

int main(int argc, char* argv[]) {

    if ( argc != 2 )
        return 0;
    std::ifstream file(argv[1], std::ios_base::in);
    if (file.good() ) {
        file.unsetf(std::ios::skipws);

        spirit::istream_iterator begin(file);
        spirit::istream_iterator end;

        keys_and_values<spirit::istream_iterator > p;
        std::map<std::string, double> m;
		bool result = qi::parse(begin, end, p, m);

        if ( !result ) {
            std::cout << "Parsing failed\n";
        }
        else {
            std::cout << "-------------------------------- \n";
            std::cout << "Parsing succeeded, found entries:" << m.size() << std::endl;
			map_d_type::iterator end = m.end();
			for ( map_d_type::iterator it=m.begin(); it != end; ++it) {
                std::cout << (*it).first << "=" << (*it).second << std::endl;          
            }
            std::cout << "---------------------------------\n";
        }
    } else {
        std::cout << "Problem with file\n" ;
    }
    if ( file.is_open() )
        file.close();
     return 0;
}
